//
//  ChangeTextViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/05/21.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import RealmSwift

class ChangeTextViewController: UIViewController{

    @IBOutlet weak var textView: UITextView!
    
    var bookData:RealmBookData! //絶対値が入っている
    var frontVC:BookShelfDetailViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.text = bookData.dataImpression
        self.navigationItem.title = "感想"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func saveButton(_ sender: UIBarButtonItem) {
        self.textView.endEditing(true)
        
        let alert = UIAlertController(title: "", message: "保存しました", preferredStyle: .alert)
        self.present(alert, animated: true, completion: {// アラートを閉じる
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        })
        
        let realm = try! Realm()
        try! realm.write { //Realm書き込み　見づらいかも
            bookData.setValue(textView.text, forKey: "dataImpression")
        }
        bookTableView.reloadData()
        frontVC.setCellData(book: bookData)
    }
}
