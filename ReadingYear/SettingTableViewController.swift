//
//  SettingTableViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/06/03.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import MessageUI

class SettingTableViewController: UITableViewController,MFMailComposeViewControllerDelegate{
    
    //chartTargetValue,chartYAxisValue Int型　使ってはいけないuserdefault
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //遷移先から戻るの時の名前をbackから戻るに変更
        let backButtonItem = UIBarButtonItem(title: "戻る", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem
        
        
        let selectedBackgroundView: UIView = UIView()
        selectedBackgroundView.backgroundColor = UIColor(red: 0, green: 135/255, blue: 1, alpha: 0.3)
        
        var indexPath:IndexPath = IndexPath(row: 1, section: 0)
        var cell:UITableViewCell = tableView(tableView, cellForRowAt: indexPath)
        cell.selectedBackgroundView = selectedBackgroundView
        indexPath = IndexPath(row: 0, section: 1)
        cell = tableView(tableView, cellForRowAt: indexPath)
        cell.selectedBackgroundView = selectedBackgroundView
        for i in 0..<2{
            let indexPath:IndexPath = IndexPath(row: i, section: 2)
            let cell:UITableViewCell = tableView(tableView, cellForRowAt: indexPath)
            cell.selectedBackgroundView = selectedBackgroundView
        }
        
        let color:UIColor=UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        tableView.backgroundColor=color
        
        
        indexPath = IndexPath(row: 0, section: 0)
        cell = self.tableView(self.tableView, cellForRowAt: indexPath)
        let switchButton = UISwitch()
        if let deleteOnOff:Bool = UserDefaults.standard.object(forKey: "bookDeleteOnOff") as? Bool{
            switchButton.setOn(deleteOnOff, animated: false) 
        }
        switchButton.addTarget(self, action: #selector(self.onOffSwitch), for: UIControl.Event.valueChanged)
        cell.accessoryView = switchButton
        switchButton.sizeToFit()
        
        let versionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        let version: String? = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        versionLabel.text = "アプリのバージョン　" + version!
        versionLabel.font = UIFont.systemFont(ofSize: 14)
        versionLabel.textAlignment = .center
        versionLabel.textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
        tableView.tableFooterView = versionLabel
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func onOffSwitch(){
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.tableView(self.tableView, cellForRowAt: indexPath)
        let switchButton = cell.accessoryView as! UISwitch
        UserDefaults.standard.setValue(switchButton.isOn, forKey: "bookDeleteOnOff")
    }
    
    //此処より下tableviewの設定
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 2
        }else if(section == 1){
            return 1
        }else{
            return 2
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 1 && indexPath.row == 0){
            let url = URL(string: "https://icons8.com")
            if UIApplication.shared.canOpenURL(url! as URL){
                UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }else if(indexPath.section == 2 && indexPath.row == 0){
            if let url = URL(string: "https://itunes.apple.com/jp/app/id1247081342?mt=8&action=write-review"){
                UIApplication.shared.open(url)
            }
//            //この遅延処理を書かないとcellが選択された状態で止まってしまう。
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1){
//                self.tableView.deselectRow(at: [0,4], animated: true)
//            }
        }else if(indexPath.section == 2 && indexPath.row == 1){
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients(["yanagi_developer@yahoo.co.jp"]) // 宛先アドレス
                mail.setSubject("eBooksお問い合わせ") // 件名
                mail.setMessageBody("", isHTML: false) // 本文
                present(mail, animated: true, completion: nil)
            } else {
                print("送信できません")
            }
        }
    }
    
    //アプリ改善の要望のための関数
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            print("キャンセル")
        case .saved:
            print("下書き保存")
        case .sent:
            print("送信成功")
        default:
            print("送信失敗")
        }
        dismiss(animated: true, completion: nil)
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
