//
//  BookTableViewCell.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/05/02.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import SwipeCellKit

class BookTableViewCell: SwipeTableViewCell {

    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookTitle: UILabel!
    @IBOutlet weak var bookAuthor: UILabel!
    @IBOutlet weak var bookStatus: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
