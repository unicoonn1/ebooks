//
//  GraphViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2018/05/19.
//  Copyright © 2018年 高柳亮太. All rights reserved.
//

import UIKit
import Charts
import RealmSwift

class GraphViewController: UIViewController {

    
    @IBOutlet weak var barChartView: BarChartView!
    var text:String = ""//今年か昨年というテキストが入ってる
    var year:String = ""//今年か昨年の数字が入る(2017,2018)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ナビゲーションバーのタイトルを設定
        self.navigationItem.title = text
        
        setChart()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setChart() {
        var months: [String] = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
        var unitsSold = [5.0,2.0,8.0,4.0,5.0,6.0,3.0,8.0,7.0,10.0,8.0,4.0]//y軸の値を収納
        
        for i in 0..<12{
            months[i] = year + "/" + months[i]
        }
        let realm = try! Realm()
        let noDataCount = realm.objects(RealmBookData.self).count
        var noDataFlag:Bool = false
        if(noDataCount == 0){
            noDataFlag = true
        }
        let chartTitle = String(realm.objects(RealmBookData.self).filter("dataRegistDate BEGINSWITH '\(year)'").count)
        for i in 0..<12{
            let result = realm.objects(RealmBookData.self).filter("dataRegistDate BEGINSWITH '\(months[i])'")
            unitsSold[i] = Double(result.count)
        }
        
        
        barChartView.leftAxis.labelCount = 6
        barChartView.rightAxis.labelCount = 6
        
        
        barChartView.animate(yAxisDuration: 2.0)
        barChartView.pinchZoomEnabled = false
        barChartView.doubleTapToZoomEnabled = false
        barChartView.dragEnabled = false
        barChartView.drawBarShadowEnabled = false
        //周りの枠組みが色濃くなる
        barChartView.drawBordersEnabled = false
        //タイトル
        barChartView.chartDescription?.text = ""
        //背景がグレーになる
        barChartView.drawGridBackgroundEnabled = true
        //タップ時のハイライトを消す処理
        barChartView.highlightPerTapEnabled = false
        barChartView.highlightPerDragEnabled = false
        
        //右ラベルを非表示
        //        barChartView.rightAxis.drawLabelsEnabled = false
        //yの最低値を0に設定
        barChartView.rightAxis.axisMinimum = 0.0
        barChartView.leftAxis.axisMinimum = 0.0
        //y軸の値を整数値にする処理。
        barChartView.rightAxis.granularityEnabled = true
        barChartView.leftAxis.granularityEnabled = true
        barChartView.rightAxis.granularity = 1.0
        barChartView.leftAxis.granularity = 1.0
        
        //xy軸スケール拡大縮小できなくする。
        barChartView.scaleXEnabled = false
        barChartView.scaleYEnabled = false
        
        var dataEntries: [BarChartDataEntry] = []
        //グラフのバーのxとyを決めてる
        for i in 0..<unitsSold.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: unitsSold[i])
            dataEntries.append(dataEntry)
        }
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "本の登録数(" + chartTitle + "冊)")
        //バーのラベル非表示
        chartDataSet.drawValuesEnabled = true
        chartDataSet.valueFormatter = BarChartValueFormatter()
        chartDataSet.colors = [UIColor(red: 1, green: 150/255, blue: 0, alpha: 1)]
        let chartData = BarChartData(dataSet: chartDataSet)
        if(noDataFlag){
            barChartView.noDataFont = UIFont.systemFont(ofSize: 18) //Noデータ時の表示フォント
            barChartView.noDataTextColor = UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1)
            barChartView.noDataText = "「本を検索」から本を登録しよう！" //Noデータ時に表示する文字
            barChartView.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
            barChartView.data = nil
        }else{
            barChartView.backgroundColor = UIColor.white
            //グラフの背景色(グレーに設定)
            //            barChartView.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
            barChartView.data = chartData
        }
        
        // X軸のラベルを設定
        let xaxis = XAxis()
        xaxis.valueFormatter = BarChartFormatter()
        barChartView.xAxis.valueFormatter = xaxis.valueFormatter
        // x軸のラベルをボトムに表示
        barChartView.xAxis.labelPosition = .bottom
        // x軸のラベルの数を設定
        barChartView.xAxis.labelCount = 12
        // x軸の縦線を表示しない
        barChartView.xAxis.drawGridLinesEnabled = false
        
    }
}

//理解していきたい、どういった処理なのかよく分かっていない
public class BarChartValueFormatter: NSObject, IValueFormatter{
    
    public func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String{
        return String(Int(entry.y))
    }
}


public class BarChartFormatter: NSObject, IAxisValueFormatter{
    // x軸のラベル
    var months: [String]! = ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"]
    
    // デリゲート。TableViewのcellForRowAtで、indexで渡されたセルをレンダリングするのに似てる。
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return months[Int(value)] // 0 -> Jan, 1 -> Feb...
    }
}
