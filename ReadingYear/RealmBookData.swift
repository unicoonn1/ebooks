//
//  RealmBookData.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/09/11.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import Foundation
import RealmSwift

class RealmBookData: Object{
    //後々追加の変数
    @objc dynamic var dataEvaluation:Int = 0//評価
    @objc dynamic var dataCollection:String = ""//コレクション //これと日付順でソート行えばうまくいきそう 1巻-18巻並び(2020/04/21 タグ？)
    
    //データ保存
    @objc dynamic var dataTitle:String = "" //タイトル
    @objc dynamic var dataAuthor:String = "" //著者
    @objc dynamic var dataImageLink:String = "" //イメージリンク
    @objc dynamic var dataImage:NSData = NSData() //イメージ
    @objc dynamic var dataPublisher:String = "" //出版社
    @objc dynamic var dataSalesDate:String = "" //発売日
    @objc dynamic var dataRegistDate:String = "" //登録日
    @objc dynamic var dataItemPrice:String = "" //値段
    @objc dynamic var dataItemCaption:String = "" //あらすじ
    @objc dynamic var dataImpression:String = "" //感想
    @objc dynamic var dataAffiliateUrl:String = "" //楽天URL
    
    @objc dynamic var dataRegistNumber:Int = 0 //登録した順にソートをさせるための変数　本棚や読みたい本に追加した順を記憶。
    @objc dynamic var dataReadStartDay:String = ""//読み始めた日
    @objc dynamic var dataReadEndDay:String = ""//読み終えた日
    @objc dynamic var dataSize:String = "" //文庫とかコミックとか本のサイズ
    @objc dynamic var dataISBN:String = "" //ISBN
    @objc dynamic var dataLabel:String = "" //レーベル
    @objc dynamic var dataStatus:String = "既読" //ステータス
}
