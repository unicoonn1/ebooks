//
//  BookSearchViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/09/13.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import RealmSwift


class BookSearchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIGestureRecognizerDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var SearchHistoryArray:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        //遷移先から戻るの時の名前をbackから戻るに変更
        let backButtonItem = UIBarButtonItem(title: "戻る", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem

        tableView.tableFooterView = UIView(frame: .zero)
        let color:UIColor=UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        tableView.backgroundColor=color
        
        if let searchHistory:[String] = UserDefaults.standard.stringArray(forKey: "SearchHistoryArray"){
            SearchHistoryArray = searchHistory
        }
        
        //searchControllerの周りのグレーを薄いグレーに変える処理。
        searchBar.barTintColor=UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1)
        //searchControllerの枠を黒から薄いグレーに変える処理。
        searchBar.layer.borderColor=UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1).cgColor
        searchBar.layer.borderWidth=1.0
        
        if #available(iOS 11.0, *) {
            searchBar.frame.size = CGSize(width: self.view.bounds.width, height: 56)
        }

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //セルの選択状態を解除する処理。　戻った時にどのセルを選択していたかを見やすくするための処理。
        if let indexPathForSelectedRow = self.tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPathForSelectedRow, animated: true)
        }
    }

    
    // フォーカスが当たる際に呼び出されるメソッド(編集の可否を定義可能).
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    // フォーカスが外れる際に呼び出されるメソッド(編集終了の可否を定義可能).
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    // キーボードの検索キータップ時に呼び出されるメソッド.
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if SearchHistoryArray.firstIndex(of: searchBar.text!) == nil {
            SearchHistoryArray.insert(searchBar.text!, at: 0)
            UserDefaults.standard.set(SearchHistoryArray, forKey: "SearchHistoryArray")
        }

        self.performSegue(withIdentifier: "Search", sender: nil)//segueを実行
        searchBar.resignFirstResponder()
        searchBar.text?.removeAll()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    // キャンセルボタンタップ時に呼び出されるメソッド.
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text?.removeAll()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    //http://qiita.com/ponkichi4/items/bbb8b2bc089170f0bcb0
    //searchbarを扱いキーボードを表示させた後、searchBar以外をタップでキーボードを下げるための処理。
    override func viewDidAppear(_ animated: Bool) {
        // ジェスチャー追加：画面タップでキーボードを閉じる
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)
        // 後述のイベント切り分け用
        tapGesture.delegate = self
    }
    // キーボードを閉じる
    @objc func closeKeyboard(_ sender: UITapGestureRecognizer){
        searchBar.resignFirstResponder()
        searchBar.text?.removeAll()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        // キーボード表示中のみ、タップジェスチャーを実行する
        // 表示中でないなら、SubViewにイベントを移譲
        if (searchBar.isFirstResponder) {
            return true
        } else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0 && indexPath.row == 0){
            let storyboard: UIStoryboard = self.storyboard!
            let navigationView = storyboard.instantiateViewController(withIdentifier: "BarCode") as! UINavigationController
            self.present(navigationView, animated: true, completion: nil)
            
            tableView.deselectRow(at: indexPath, animated: true)
        }else if(indexPath.section == 0 && indexPath.row == 1){
            self.performSegue(withIdentifier: "DetailSearch", sender: nil)//segueを実行
        }else{
            searchBar.text = SearchHistoryArray[indexPath.row]
            self.performSegue(withIdentifier: "Search", sender: nil)//segueを実行
            searchBar.text?.removeAll()
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 1){
            return "検索履歴"
        }else{
            return ""
        }
    }
    
    public func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if(section == 1){
            if(SearchHistoryArray.isEmpty){
                return ""
            }
            return "スワイプで履歴削除"
        }else{
            return ""
        }
    }
    
    //セルの個数を指定するデリゲートメソッド(必須)
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(section == 0){
            return 2
        }else{
            return SearchHistoryArray.count
        }
    }
    
    //セルに値を設定するデータソースメソッド(必須)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "basic", for: indexPath)
        if(indexPath.section == 0 && indexPath.row == 0){
            cell.imageView?.image = UIImage(named: "BarCode")
            cell.textLabel?.text = "バーコードから書籍追加"
            cell.separatorInset = UIEdgeInsets.zero
            cell.accessoryType = .none
        }else if(indexPath.section == 0 && indexPath.row == 1){
            cell.imageView?.image = UIImage(named: "Search")
            cell.textLabel?.text = "詳細条件を指定して検索"
            cell.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 0)
            cell.accessoryType = .disclosureIndicator
        }else{
            cell.imageView?.image = nil
            cell.textLabel?.text = SearchHistoryArray[indexPath.row]
            cell.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 0)
            cell.accessoryType = .none
        }
        //セルのハイライト色をグレーから薄い青に変更する処理。
        let selectedBackgroundView: UIView = UIView()
        selectedBackgroundView.backgroundColor = UIColor(red: 0, green: 135/255, blue: 1, alpha: 0.3)
        cell.selectedBackgroundView = selectedBackgroundView
        return cell
    }
    
    func tableView(_ tableView: UITableView,canEditRowAt indexPath: IndexPath) -> Bool{
        if(indexPath.section == 0){
            return false
        }else{
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            SearchHistoryArray.remove(at: indexPath.row)
            UserDefaults.standard.set(SearchHistoryArray, forKey: "SearchHistoryArray")

            tableView.deleteRows(at: [indexPath], with: .fade)
            if(SearchHistoryArray.isEmpty){
                tableView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Search" {
            let searchView:SearchViewController = segue.destination as! SearchViewController
            searchView.searchBarText = searchBar.text!
            searchView.frontVC = self
        }else if(segue.identifier == "DetailSearch"){
            let _:DetailSearchTableViewController = segue.destination as! DetailSearchTableViewController
        }
    }
}
