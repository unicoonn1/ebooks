//
//  BookTableViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/09/28.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import RealmSwift
import DZNEmptyDataSet
import StoreKit
import SwipeCellKit

var bookTableView:UITableView = UITableView()


class BookTableViewController: UITableViewController,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource {
    
    public var searchText: String = ""
    public var filterText: String = "全て"
    public var sortText: String = "dataRegistNumber"
    private var tagText: String = ""
    private var str:[String] = []
    
    //tagのためのtextField
    private var textField: UITextField!
    //このリストにタグの履歴のデータを入れる。
    private var tagHistoryArray:[String] = []

    //書籍詳細画面で本を削除を選択した場合に利用
    public var bookDeleteFlag = false
    private var selectCellIndexPath = IndexPath(row: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        tableView.tableFooterView = UIView(frame: .zero)
        let color:UIColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        tableView.backgroundColor = color
    
        //遷移先から戻るの時の名前をbackから戻るに変更
        let backButtonItem = UIBarButtonItem(title: "戻る", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem
        
        bookTableView = self.tableView
        
        //画面遷移した時にsearchBarを消す処理。
        self.definesPresentationContext = true //いらない

        //navigationBarタイトルの生成処理
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        let searchButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        searchButton.addTarget(self, action: #selector(tagButtonTap), for: .touchUpInside)
        view.addSubview(searchButton)
        let label = UILabel(frame: CGRect(x: 0, y: 13, width: 44, height: 18))
        label.text = "本棚"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = UIColor.white
        view.addSubview(label)
        let imageView = UIImageView(frame: CGRect(x: 17, y: 30, width: 10, height: 10))
        imageView.image = UIImage(named: "icons8-sort-down-filled-50.png")
        view.addSubview(imageView)
        navigationItem.titleView = view
        
        //レビューの依頼
        let realm = try! Realm()
        let result = realm.objects(RealmBookData.self)
        if result.count >= 3 {
            if let reviewFlag:Bool = UserDefaults.standard.object(forKey: "reviewFlag") as? Bool{
                if(reviewFlag == false){
                    SKStoreReviewController.requestReview()
                    UserDefaults.standard.set(true, forKey: "reviewFlag")
                }
            }else{
                UserDefaults.standard.set(false, forKey: "reviewFlag")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        self.tabBarController?.tabBar.isHidden = false //不要な処理かもしれない
        tableView.contentInset.bottom = 0
        tableView.scrollIndicatorInsets.bottom = 0
        
        tableView.emptyDataSetSource = self//なぜここで宣言？
        tableView.emptyDataSetDelegate = self
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        let image:UIImage = UIImage(named: "icons8-reading-filled-100.png")!
        return image
    }
    func imageTintColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return UIColor.lightGray
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "本の登録数は0冊です"
        return NSAttributedString(string: text)
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "「本を検索」から本を登録しよう！\n※本は左スワイプで登録できます"
        return NSAttributedString(string: text)
    }
        
    //戻った時にセルの選択状態を解除する処理。
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //TODO
        
        //セルの選択状態を解除する処理。　戻った時にどのセルを選択していたかを見やすくするための処理。
        //TODO 削除フラグが立っていた場合には、本をdeleteする処理を行う。
        if (bookDeleteFlag) {
            let realm = try! Realm()
            var result = realm.objects(RealmBookData.self)
            if self.searchText != "" {
                result = result.filter("dataTitle CONTAINS '\(self.searchText)' || dataAuthor CONTAINS '\(self.searchText)' ||  dataPublisher CONTAINS '\(self.searchText)'")
            }else{
                result = self.resultData(indexPath: selectCellIndexPath)
            }
            
            try! realm.write {
                realm.delete(result[selectCellIndexPath.row])
            }
            let alert = UIAlertController(title: "", message: "本を削除しました", preferredStyle: .alert)
            self.present(alert, animated: true, completion: {// アラートを閉じる
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if(result.count == 0){
                        self.tableView.reloadData()
                    }else{
                        self.tableView.deleteRows(at: [self.selectCellIndexPath], with: .fade)
                    }
                    alert.dismiss(animated: true, completion: nil)
                })
            })
            bookDeleteFlag = false
        }else{
            tableView.deselectRow(at: selectCellIndexPath, animated: true)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {//sectionの数
        if(sortText == "dataCollection"){//ここ超汚い気がする　処理
            self.str = []
            let realm = try! Realm()
            var result:Results<RealmBookData> = realm.objects(RealmBookData.self).sorted(byKeyPath: "dataCollection", ascending: true)
            self.str.append(result[0].dataCollection)//0個の場合クラッシュするのでは？
            var query:String = ""
            if(filterText == "全て"){
                query = "dataCollection != '\(result[0].dataCollection)'"
            }else{
                query = "dataStatus = '\(filterText)' && dataCollection != '\(result[0].dataCollection)'"
            }
            result = realm.objects(RealmBookData.self).filter(query).sorted(byKeyPath: "dataCollection", ascending: true)
            while(result.count != 0){
                self.str.append(result[0].dataCollection)
                query = "\(query) && dataCollection != '\(result[0].dataCollection)'"
                result = realm.objects(RealmBookData.self).filter(query).sorted(byKeyPath: "dataCollection", ascending: true)
            }
            return str.count
        }else{
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {//sectionのタイトル設定
        if (sortText == "dataCollection") {
            return str[section]
        }else{
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(sortText == "dataCollection"){
            let label : UILabel = UILabel()
            label.backgroundColor = UIColor(red: 0, green: 160/255, blue: 1, alpha: 1)
            label.textColor = UIColor.white
            label.font = UIFont.systemFont(ofSize: 14)//UIのフォントとサイズ
            if(str[section] == ""){
                label.text = "  未設定"
            }else{
                label.text = "  " + str[section]
            }
            return label
        }else{
            return UIView()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (sortText == "dataCollection") {
            return 28
        }else{
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let realm = try! Realm()
        let result = realm.objects(RealmBookData.self)
        let indexPath = IndexPath(row: 0, section: section)
        if searchText != "" {
            return result.filter("dataTitle CONTAINS '\(searchText)' || dataAuthor CONTAINS '\(searchText)' || dataPublisher CONTAINS '\(searchText)'").count
        }else if(tagText != ""){
            return result.filter("dataCollection == '\(tagText)' AND dataCollection BEGINSWITH '\(tagText)'").count
        }else{
            return resultData(indexPath: indexPath).count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookCell",for: indexPath) as! BookTableViewCell
        cell.delegate = self
        
        let realm = try! Realm()
        var result = realm.objects(RealmBookData.self)
        if searchText != "" {
            result = result.filter("dataTitle CONTAINS '\(searchText)' || dataAuthor CONTAINS '\(searchText)' ||  dataPublisher CONTAINS '\(searchText)'")
        }else if(tagText != ""){
            result = result.filter("dataCollection == '\(tagText)' AND dataCollection BEGINSWITH '\(tagText)'")
        }else{
            result = resultData(indexPath: indexPath)
        }
        let book = result[indexPath.row]
        
        cell.bookImage.image = UIImage(data: book.dataImage as Data)
        cell.bookTitle.text = book.dataTitle
        cell.bookAuthor.text = book.dataAuthor
        
        if(book.dataStatus == "既読"){
            cell.bookStatus.image = UIImage()
        }else if(book.dataStatus == "未読"){
            cell.bookStatus.image = UIImage(named: "BookStack.png")
        }else if(book.dataStatus == "欲しい"){
            cell.bookStatus.image = UIImage(named: "Star.png")
        }else{
            cell.bookStatus.image = UIImage(named: "BookRead.png")
        }
        
        //セルのハイライト色をグレーから薄い青に変更する処理。
        let selectedBackgroundView: UIView = UIView()
        selectedBackgroundView.backgroundColor = UIColor(red: 0, green: 135/255, blue: 1, alpha: 0.3)
        cell.selectedBackgroundView = selectedBackgroundView
        
        return cell
    }
    
    //タグの履歴を保存する処理。
    func tagHistoryArraySave(text: String){
        if(tagHistoryArray.firstIndex(of: text) == nil && text != ""){//履歴にないデータ　かつ　未入力は入れない
            tagHistoryArray.insert(text, at: 0)
            UserDefaults.standard.set(tagHistoryArray, forKey: "TagHistoryArray")
        }
    }
    
    //遷移先に値を渡す処理
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let bookDetailView:BookShelfDetailViewController = segue.destination as! BookShelfDetailViewController
        if let indexPath = self.tableView.indexPathForSelectedRow{
            let realm = try! Realm()
            var result = realm.objects(RealmBookData.self)
            if searchText != "" {
                result = result.filter("dataTitle CONTAINS '\(searchText)' || dataAuthor CONTAINS '\(searchText)' ||  dataPublisher CONTAINS '\(searchText)'")
            }else{
                result = resultData(indexPath: indexPath)
            }
            let bookData = result[indexPath.row]
            bookDetailView.bookData = bookData
            self.selectCellIndexPath = indexPath
        }
    }
    
    //TODO: この関数に全てのrealmdata取得関連の関数を集約する。
    func resultData(indexPath: IndexPath) -> Results<RealmBookData>{
        let realm = try! Realm()
        var result:Results<RealmBookData> = realm.objects(RealmBookData.self)
        
        if(filterText == "全て"){//全ての場合はfilterをかけない
            if(sortText == "dataRegistNumber"){
                result = result.sorted(byKeyPath: "\(sortText)", ascending: false)
            }else if(sortText == "dataCollection"){
                result = result.filter("dataCollection = '\(str[indexPath.section])'").sorted(byKeyPath: "dataRegistNumber", ascending: false)
            }else{
                result = result.sorted(byKeyPath: "\(sortText)", ascending: true)
            }
        }else{//filterかかる場合の時の処理。
            if(sortText == "dataRegistNumber"){
                result = result.filter("dataStatus = '\(filterText)'").sorted(byKeyPath: "\(sortText)", ascending: false)
            }else if(sortText == "dataCollection"){
                result = result.filter("dataStatus = '\(filterText)' && dataCollection = '\(str[indexPath.section])'").sorted(byKeyPath: "dataRegistNumber", ascending: false)
            }else{
                result = result.filter("dataStatus = '\(filterText)'").sorted(byKeyPath: "\(sortText)", ascending: true)
            }
        }
        return result
    }
}

//TODO: アラートコントローラ関連が肥大化しすぎている。今後修正したい。
//MARK: - ButtonActions
extension BookTableViewController: UITextFieldDelegate{
    //画面切り替え処理　グリッド表示
    @IBAction func changeViewButton(_ sender: UIBarButtonItem) {
        let VC = self.tabBarController?.children[0] as? ViewController
        VC?.changeContainerView()
    }

    //検索ボタンタップ時処理
    @IBAction func searchButton(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "本棚内検索", message: "キーワードを入力してください\n※検索後は空欄にしてください", preferredStyle: UIAlertController.Style.alert)
        
        alertController.addTextField{(textFieldA) -> Void in
            self.textField = textFieldA
            self.textField.delegate = self
            self.textField.placeholder = "タイトル、著者など"
            self.textField.textAlignment = .center
            self.textField.isEnabled = true
            self.textField.text = self.searchText
        }
        
        let defaultAction = UIAlertAction(title: "検索",style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.filterText = "全て"
            self.sortText = "dataRegistNumber"
            self.searchText = (alertController.textFields?.first!.text)! //text_get
            self.tableView.reloadData()
        })
        let cancel = UIAlertAction(title: "キャンセル",style: UIAlertAction.Style.cancel,handler: {
            (action: UIAlertAction!) -> Void in
        })
        
        alertController.addAction(defaultAction)
        alertController.addAction(cancel)
        self.present(alertController,animated: true,completion: {
            alertController.textFields?.first?.isEnabled = true
        })
    }

    // タグボタンタップ時処理
    //TODO タグで処理を制御する処理を追加する
    @objc func tagButtonTap(){
        if let tagHistory:[String] = UserDefaults.standard.stringArray(forKey: "TagHistoryArray"){
            self.tagHistoryArray = tagHistory
        }
        let alertController = UIAlertController(title: "タグで絞り込み検索", message: "選択してください。", preferredStyle: UIAlertController.Style.alert)
        
        for tag in tagHistoryArray{
            let defaultAction = UIAlertAction(title: tag,style: UIAlertAction.Style.default,handler: {
                (action: UIAlertAction!) -> Void in
                self.tagText = tag
                self.tableView.reloadData()
            })
            alertController.addAction(defaultAction)
        }
        let cancelAction = UIAlertAction(title: "キャンセル",style: UIAlertAction.Style.cancel,handler: {
            (action: UIAlertAction!) -> Void in
            
        })
        alertController.addAction(cancelAction)
        self.present(alertController,animated: true,completion: nil)
    }
    
    //フィルターボタンタップ時処理
    @IBAction func filterButton(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "絞り込み", message: .none, preferredStyle: UIAlertController.Style.actionSheet)
        var titleName:[String] = ["全て","欲しい","読んでいる","未読","既読"]
        
        let defaultAction = UIAlertAction(title: titleName[0],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.filterText = "全て"
            self.tableView.reloadData()
        })
        let defaultAction1 = UIAlertAction(title: titleName[1],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.filterText = "欲しい"
            self.tableView.reloadData()
        })
        let defaultAction2 = UIAlertAction(title: titleName[2],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.filterText = "読んでいる"
            self.tableView.reloadData()
        })
        let defaultAction3 = UIAlertAction(title: titleName[3],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.filterText = "未読"
            self.tableView.reloadData()
        })
        let defaultAction4 = UIAlertAction(title: titleName[4],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.filterText = "既読"
            self.tableView.reloadData()
        })
        let cancel = UIAlertAction(title: "キャンセル",style: UIAlertAction.Style.cancel,handler: {
            (action: UIAlertAction!) -> Void in
        })
        if(self.filterText == "全て"){
            defaultActionAlert(action: defaultAction)
        }else if(self.filterText == "欲しい"){
            defaultActionAlert(action: defaultAction1)
        }else if(self.filterText == "読んでいる"){
            defaultActionAlert(action: defaultAction2)
        }else if(self.filterText == "未読"){
            defaultActionAlert(action: defaultAction3)
        }else{
            defaultActionAlert(action: defaultAction4)
        }
        
        alertController.addAction(defaultAction)
        alertController.addAction(defaultAction1)
        alertController.addAction(defaultAction2)
        alertController.addAction(defaultAction3)
        alertController.addAction(defaultAction4)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: nil)
    }
    
    //ソートボタンタップ時処理
    @IBAction func sortButton(_ sender: UIBarButtonItem) {
        let alert: UIAlertController = UIAlertController(title: "並び替え", message: .none, preferredStyle:  UIAlertController.Style.actionSheet)
        var titleName:[String] = ["登録順","タイトル順","著者順","タグ順"]
        
        let defaultAction: UIAlertAction = UIAlertAction(title: titleName[0], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.sortText = "dataRegistNumber"
            self.tableView.reloadData()
        })
        
        let defaultAction1: UIAlertAction = UIAlertAction(title: titleName[1], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.sortText = "dataTitle"
            self.tableView.reloadData()
        })
        let defaultAction2: UIAlertAction = UIAlertAction(title: titleName[2], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.sortText = "dataAuthor"
            self.tableView.reloadData()
        })
        let defaultAction3: UIAlertAction = UIAlertAction(title: titleName[3], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.sortText = "dataCollection"
            self.tableView.reloadData()
        })
        let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler:{
            (action: UIAlertAction!) -> Void in
        })
        
        if(self.sortText == "dataRegistNumber"){
            defaultActionAlert(action: defaultAction)
        }else if(self.sortText == "dataTitle"){
            defaultActionAlert(action: defaultAction1)
        }else if(self.sortText == "dataAuthor"){
            defaultActionAlert(action: defaultAction2)
        }else{
            defaultActionAlert(action: defaultAction3)
        }
        
        alert.addAction(defaultAction)
        alert.addAction(defaultAction1)
        alert.addAction(defaultAction2)
        alert.addAction(defaultAction3)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func defaultActionAlert(action: UIAlertAction){
        action.setValue(UIImage(named: "todo.png"), forKey: "image")
        action.setValue(UIColor.orange, forKey: "imageTintColor")
        action.setValue(UIColor.orange, forKey: "titleTextColor")
    }
}



//MARK: - SwipeTableViewCellDelegate
extension BookTableViewController: SwipeTableViewCellDelegate{
    /*ver4.0.1 SwipeAction*/
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let editAction = SwipeAction(style: .destructive, title: "編集"){ actionA, indexPath in
            actionA.fulfill(with: .reset)
            
            let realm = try! Realm()
            var result = realm.objects(RealmBookData.self)
            if self.searchText != "" {
                result = result.filter("dataTitle CONTAINS '\(self.searchText)' || dataAuthor CONTAINS '\(self.searchText)' ||  dataPublisher CONTAINS '\(self.searchText)'")
            }else{
                result = self.resultData(indexPath: indexPath)
            }
            
            let alert: UIAlertController = UIAlertController(title: "本を編集", message: "タグと読書状況を編集してください", preferredStyle:  UIAlertController.Style.alert)
            var titleName:[String] = ["欲しい","読んでいる","未読","既読"]
            alert.addTextField{(textFieldA: UITextField) -> Void in
                self.textField = textFieldA
                self.textField.delegate = self
                self.textField.text = result[indexPath.row].dataCollection
                self.textField.placeholder = "タグを入力してください"
                self.textField.textAlignment = .center
                self.textField.isEnabled = false
                
                let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
                let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
                let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                let history = UIBarButtonItem(title: "履歴", style: .plain, target: self, action: #selector(self.history))
                toolbar.setItems([doneItem,flexibleItem,history], animated: true)
                self.textField.inputAccessoryView = toolbar
                
                if let tagHistory:[String] = UserDefaults.standard.stringArray(forKey: "TagHistoryArray"){
                    self.tagHistoryArray = tagHistory
                }
            }
            let defaultAction: UIAlertAction = UIAlertAction(title: titleName[0], style: UIAlertAction.Style.default, handler:{
                (action: UIAlertAction!) -> Void in
                try! realm.write {
                    result[indexPath.row].dataStatus = titleName[0]
                    result[indexPath.row].dataCollection = (alert.textFields?.first?.text)!
                }
                self.tagHistoryArraySave(text: (alert.textFields?.first?.text)!)
                self.tableView.reloadData()
            })
            
            let defaultAction1: UIAlertAction = UIAlertAction(title: titleName[1], style: UIAlertAction.Style.default, handler:{
                (action: UIAlertAction!) -> Void in
                try! realm.write {
                    result[indexPath.row].dataStatus = titleName[1]
                    result[indexPath.row].dataCollection = (alert.textFields?.first?.text)!
                }
                self.tagHistoryArraySave(text: (alert.textFields?.first?.text)!)
                self.tableView.reloadData()
            })
            let defaultAction2: UIAlertAction = UIAlertAction(title: titleName[2], style: UIAlertAction.Style.default, handler:{
                (action: UIAlertAction!) -> Void in
                try! realm.write {
                    result[indexPath.row].dataStatus = titleName[2]
                    result[indexPath.row].dataCollection = (alert.textFields?.first?.text)!
                }
                self.tagHistoryArraySave(text: (alert.textFields?.first?.text)!)
                self.tableView.reloadData()
            })
            let defaultAction3: UIAlertAction = UIAlertAction(title: titleName[3], style: UIAlertAction.Style.default, handler:{
                (action: UIAlertAction!) -> Void in
                try! realm.write {
                    result[indexPath.row].dataStatus = titleName[3]
                    result[indexPath.row].dataCollection = (alert.textFields?.first?.text)!
                }
                self.tagHistoryArraySave(text: (alert.textFields?.first?.text)!)
                self.tableView.reloadData()
            })
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler:{
                (action: UIAlertAction!) -> Void in
                self.tableView.reloadData()
            })
            
            if(result[indexPath.row].dataStatus == titleName[0]){
                defaultAction.setValue(UIImage(named: "todo.png"), forKey: "image")
                defaultAction.setValue(UIColor.orange, forKey: "imageTintColor")
                defaultAction.setValue(UIColor.orange, forKey: "titleTextColor")
            }else if(result[indexPath.row].dataStatus == titleName[1]){
                defaultAction1.setValue(UIImage(named: "todo.png"), forKey: "image")
                defaultAction1.setValue(UIColor.orange, forKey: "imageTintColor")
                defaultAction1.setValue(UIColor.orange, forKey: "titleTextColor")
            }else if(result[indexPath.row].dataStatus == titleName[2]){
                defaultAction2.setValue(UIImage(named: "todo.png"), forKey: "image")
                defaultAction2.setValue(UIColor.orange, forKey: "imageTintColor")
                defaultAction2.setValue(UIColor.orange, forKey: "titleTextColor")
            }else{
                defaultAction3.setValue(UIImage(named: "todo.png"), forKey: "image")
                defaultAction3.setValue(UIColor.orange, forKey: "imageTintColor")
                defaultAction3.setValue(UIColor.orange, forKey: "titleTextColor")
            }
            
            alert.addAction(defaultAction)
            alert.addAction(defaultAction1)
            alert.addAction(defaultAction2)
            alert.addAction(defaultAction3)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: {
                alert.textFields?.first?.isEnabled = true
            })
        }
        editAction.backgroundColor = UIColor(red: 0, green: 220/255, blue: 50/255, alpha: 1)
        editAction.image = UIImage(named: "editImage")
        
        let deleteAction = SwipeAction(style: .destructive, title: "削除"){ action, indexPath in
            action.fulfill(with: .reset)
            
            let realm = try! Realm()
            var result = realm.objects(RealmBookData.self)
            if self.searchText != "" {
                result = result.filter("dataTitle CONTAINS '\(self.searchText)' || dataAuthor CONTAINS '\(self.searchText)' ||  dataPublisher CONTAINS '\(self.searchText)'")
            }else{
                result = self.resultData(indexPath: indexPath)
            }
            
            var bookDeleteOnOff:Bool = false
            if let deleteOnOff:Bool = UserDefaults.standard.object(forKey: "bookDeleteOnOff") as? Bool{
                bookDeleteOnOff = deleteOnOff
            }
            if(bookDeleteOnOff){
                let alert: UIAlertController = UIAlertController(title: "削除", message: "本当に削除してよろしいですか？", preferredStyle:  UIAlertController.Style.alert)
                
                let defaultAction: UIAlertAction = UIAlertAction(title: "削除", style: UIAlertAction.Style.destructive, handler:{
                    (action: UIAlertAction!) -> Void in
                    try! realm.write {
                        realm.delete(result[indexPath.row])
                    }
                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                    if(result.count == 0){
                        tableView.reloadData()
                    }
                })
                
                let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler:{
                    (action: UIAlertAction!) -> Void in
                    self.tableView.reloadData()
                })
                
                alert.addAction(defaultAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }else{
                try! realm.write {
                    realm.delete(result[indexPath.row])
                }
                tableView.deleteRows(at: [indexPath], with: .fade)
                if(result.count == 0){
                    tableView.reloadData()
                }
            }
        }
        deleteAction.backgroundColor = UIColor.red
        deleteAction.image = UIImage(named: "dustelboxImage")
        
        return [deleteAction,editAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive
        options.transitionStyle = .border
        return options
    }
    /***************************************/
}



//MARK: - タグの履歴表示関数まとめ
extension BookTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //表示する数
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return tagHistoryArray.count + 1
    }
    //表示するためのもの
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (row == 0) {
            return ""
        }else{
            return tagHistoryArray[row - 1]
        }
    }
    //選択された時
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row != 0 {
            textField.text = tagHistoryArray[row - 1]
        }
    }
    
    @objc func done() {
        textField.endEditing(true)
    }
    
    @objc func history(){
        //PickerViewの設定
        let pickerView: UIPickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.showsSelectionIndicator = true
        self.textField.inputView = pickerView
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let history = UIBarButtonItem(title: "キー", style: .plain, target: self, action: #selector(self.keyboard))
        toolbar.setItems([doneItem,flexibleItem,history], animated: true)
        self.textField.inputAccessoryView = toolbar
        
        self.textField.reloadInputViews()
    }
    
    @objc func keyboard() {
        //https://starzero.hatenablog.com/entry/2014/08/05/170122 普通のキーボードに戻す処理
        self.textField.inputView = nil
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let history = UIBarButtonItem(title: "履歴", style: .plain, target: self, action: #selector(self.history))
        toolbar.setItems([doneItem,flexibleItem,history], animated: true)
        self.textField.inputAccessoryView = toolbar
        
        self.textField.reloadInputViews()
    }
}


