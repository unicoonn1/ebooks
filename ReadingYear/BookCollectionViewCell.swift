//
//  BookCollectionViewCell.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/09/28.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit

class BookCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bookStatus: UIImageView!
    
}
