//
//  ViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/04/07.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import AVFoundation
import RealmSwift

class BarCodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate,UITextFieldDelegate {
    
    // セッションのインスタンス生成
    let captureSession = AVCaptureSession()
    var label:String=""
    
    //APIのURL
    let entryUrl: String="https://app.rakuten.co.jp/services/api/BooksBook/Search/20130522?format=json&isbn="
    let aplicationID:String="&outOfStockFlag=1&affiliateId=15be4ccf.cc97e6e6.15be4cd0.efe26c07&applicationId=1014949399335024209"
    
    
    @IBOutlet var previewView: UIView!
    
    //tagのためのtextField
    var textField: UITextField!
    //このリストにタグの履歴のデータを入れる。
    var tagHistoryArray:[String] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let videoDevice = AVCaptureDevice.default(for: AVMediaType.video)
        let videoInput:AVCaptureInput?
        
        // 入力（背面カメラ）
        do {
            videoInput = try AVCaptureDeviceInput.init(device: videoDevice!)
        } catch {
            videoInput = nil
            let alert: UIAlertController = UIAlertController(title: "カメラへのアクセスを求めています", message: "アクセスを許可してください", preferredStyle:  UIAlertController.Style.alert)
            
            let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{
                (action: UIAlertAction!) -> Void in
                let url = NSURL(string:UIApplication.openSettingsURLString)
                UIApplication.shared.open(url! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            })
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "許可しない", style: UIAlertAction.Style.cancel, handler:{
                (action: UIAlertAction!) -> Void in
            })
            
            alert.addAction(defaultAction)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
            
            return
        }
        captureSession.addInput(videoInput!)

        // 出力（ビデオデータ）
        let metadataOutput = AVCaptureMetadataOutput()
        captureSession.addOutput(metadataOutput)
        
        // メタデータを検出した際のデリゲート設定
        metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        // EAN-13コードの認識を設定
        metadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.ean13,AVMetadataObject.ObjectType.ean8]
        
        let videoLayer = AVCaptureVideoPreviewLayer.init(session: captureSession)
        videoLayer.frame = previewView.bounds
        videoLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        previewView.layer.addSublayer(videoLayer)

        // 検出エリアのビュー
        let x: CGFloat = 0.05
        let y: CGFloat = 0.3
        let width: CGFloat = 0.9
        let height: CGFloat = 0.2
        
        let detectionArea = UIView()
        detectionArea.frame = CGRect(x: view.frame.size.width * x, y: view.frame.size.height * y, width: view.frame.size.width * width, height: view.frame.size.height * height)
        detectionArea.layer.borderColor = UIColor.red.cgColor
        detectionArea.layer.borderWidth = 3
        view.addSubview(detectionArea)
        // 検出エリアの設定
        metadataOutput.rectOfInterest = CGRect(x: y,y: 1-x-width,width: height,height: width)
        // セッションの開始
        DispatchQueue.global(qos: .userInitiated).async {
            self.captureSession.startRunning()
        }
        
        let label = UILabel()
        label.frame = CGRect(x: 0, y: self.view.bounds.height - 80, width: self.view.bounds.width, height: 80)
        label.text = "978-から始まるバーコードを読み取ってください"
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 14)
        label.backgroundColor = UIColor(red: 0, green: 145/255, blue: 1, alpha: 1)
        view.addSubview(label)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func saveBookRegistNumber(){
        bookRegistNumber += 1
        let userDefaults = UserDefaults.standard
        userDefaults.set(bookRegistNumber, forKey: "bookRegistNumber")
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // 複数のメタデータを検出できる
        for metadata in metadataObjects as! [AVMetadataMachineReadableCodeObject] {
            // EAN-13Qコードのデータかどうかの確認
            if metadata.type == AVMetadataObject.ObjectType.ean13 || metadata.type == AVMetadataObject.ObjectType.ean8{
                if  metadata.stringValue != nil {
                    // 検出データを取得
                    if  label != metadata.stringValue! {
                        label = metadata.stringValue!
                        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate) // バイブレーション
                        
                        //カメラ停止とタイマーループ停止
                        self.captureSession.stopRunning()
                        
                        //パラメータをエンコードしたURL1を作成する
                        let requestUrl=entryUrl+metadata.stringValue!+aplicationID
                        
                        //APIをリクエストする
                        request(requestUrl: requestUrl,readISBN: metadata.stringValue!)
                    }
                }
            }
        }
    }
    
    func request(requestUrl: String,readISBN: String){
        //URL生成
        guard let url=URL(string: requestUrl) else{
            return
        }
        
        //リクエスト生成
        let request=URLRequest(url: url)
        let session=URLSession.shared
        let task=session.dataTask(with: request){(data:Data?,response:URLResponse?,error:Error?) in
            //通信完了後の処理
            //エラーチェック
            guard error == nil else{
                //エラー処理かく
                self.errorAlert(title: "エラー", message: "978-から始まるバーコードを読み取ってください")
                return
            }
            
            //JSONで返却されたデータをパースして格納する
            guard let data = data else{
                //データなし
                self.errorAlert(title: "エラー", message: "978-から始まるバーコードを読み取ってください")
                return
            }
            
            //JSON形式への変換処理
            guard let jsonData = try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any] else{
                //変換失敗
                self.errorAlert(title: "変換失敗", message: "変換処理に失敗しました")
                return
            }
            
            //データ解析
            guard let resultSet = jsonData["Items"] as? [[String:Any]] else{
                //データなし
                self.errorAlert(title: "エラー", message: "978-から始まるバーコードを読み取ってください")
                return
            }
            if (resultSet.isEmpty){
                //978から始まるバーコードを読み込んでいるのに表示されない場合は本がありませんと表示する。修正したい
                if readISBN.prefix(3) == "978"{
                    self.errorAlert(title: "データ無し", message: "書籍データが存在しません")
                    return
                }else{
                    self.errorAlert(title: "エラー", message: "978-から始まるバーコードを読み取ってください")
                    return
                }
            }
            
            self.parseData(resultSet:resultSet)
        }
        //通信開始
        task.resume()
    }
    
    func parseData(resultSet: [[String: Any]]){
        for item in resultSet{
            guard let volumeInfo=item["Item"] as? [String: Any] else{
                continue
            }
            let title=volumeInfo["title"] as? String
            let authors=volumeInfo["author"] as? String
            let publisher=volumeInfo["publisherName"] as? String
            let salesDate=volumeInfo["salesDate"] as? String
            let itemPrice=volumeInfo["itemPrice"] as? Int
            let itemcaption=volumeInfo["itemCaption"] as? String
            let largeImageUrl=volumeInfo["largeImageUrl"] as? String
            let affiliateUrl=volumeInfo["affiliateUrl"] as? String //アフィリエイトID
            let size = volumeInfo["size"] as? String
            let isbn = volumeInfo["isbn"] as? String
            let label = volumeInfo["seriesName"] as? String
            
            let now = NSDate() // 現在日時の取得
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "ja_JP") as Locale? // ロケールの設定
            dateFormatter.dateFormat = "yyyy/MM/dd" // 日付フォーマットの設定
            
            let book=RealmBookData()

            book.dataTitle = title! //タイトル
            book.dataAuthor = authors! //著者
            book.dataPublisher = publisher! //出版社
            book.dataSalesDate = salesDate! //発売日
            book.dataRegistDate=dateFormatter.string(from: now as Date) //登録日
            book.dataItemPrice = String(itemPrice!)+"円" //値段 ここnilだった場合危ないかも
            book.dataItemCaption = itemcaption! //あらすじ
            book.dataImageLink = largeImageUrl! //イメージリンク
            book.dataAffiliateUrl = affiliateUrl! //アフィリエイト
            book.dataSize = size!
            book.dataISBN = isbn!
            book.dataLabel = label!
            
            self.getImage(book:book) //イメージ
        }
    }
    
    func errorAlert(title:String,message:String){
        DispatchQueue.main.async {
            //アラートダイアログ生成
            let alertController=UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction=UIAlertAction(title:"OK",style: UIAlertAction.Style.cancel,handler: {
                (action: UIAlertAction!) -> Void in
                //カメラで読み込みを再開する。
                self.captureSession.startRunning()
            })
            alertController.addAction(okAction)
            self.present(alertController,animated: true,completion: nil)
        }
    }
    
    
    func getImage(book: RealmBookData){
        var bookImageUrl = book.dataImageLink
        bookImageUrl = bookImageUrl.replacingOccurrences(of: "?_ex=200x200", with: "?_ex=400x400")
        guard let url=URL(string: bookImageUrl) else{
            return
        }
        let request=URLRequest(url: url)
        let session=URLSession.shared
        let task = session.dataTask(with: request){ (data:Data?,
            response: URLResponse?,error: Error?) in
            guard error == nil else{
                return
            }
            guard let data = data else{
                return
            }
            guard let image = UIImage(data: data) else{
                return
            }
            DispatchQueue.main.async {
                book.dataImage = image.pngData()! as NSData
                //本棚か読みたい本がないかを調べてから追加。
                self.sameBookAlert(book: book)
            }
        }
        task.resume()
    }
    

    @IBAction func endView(_ sender: UIBarButtonItem) {
        bookTableView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func continueCapture(){
        let alert = UIAlertController(title: "", message: "本棚に登録しました", preferredStyle: .alert)
        self.present(alert, animated: true, completion: {// アラートを閉じる
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                alert.dismiss(animated: true, completion: nil)
                //カメラで読み込みを再開する。
                self.captureSession.startRunning()
            })
        })
    }
    
    
    func sameBookAlert(book: RealmBookData){
        var title = ""
        let realm = try! Realm()
        let bookResult:Int = realm.objects(RealmBookData.self).filter("dataISBN = '\(book.dataISBN)'").count
        if(bookResult >= 1){
            title = "本棚に登録されています"
        }
        
        if(title != ""){
            let alertController=UIAlertController(title: title, message: "新たに登録しますか？", preferredStyle: UIAlertController.Style.alert)
            let okAction=UIAlertAction(title:"OK",style: UIAlertAction.Style.default,handler: {
                (action: UIAlertAction!) -> Void in
                self.bookAlertSheet(book: book)
            })
            let cancel=UIAlertAction(title:"キャンセル",style: UIAlertAction.Style.cancel,handler: {
                (action: UIAlertAction!) -> Void in
                //カメラで読み込みを再開する。
                self.captureSession.startRunning()
            })
            alertController.addAction(okAction)
            alertController.addAction(cancel)
            self.present(alertController,animated: true,completion: nil)
        }else{
            self.bookAlertSheet(book: book)
        }
    }
    
    func editActionBookSave(book: RealmBookData,dataStatus: String,bookTagTextField: String){
        let bookData = RealmBookData()
        bookData.dataTitle = book.dataTitle
        bookData.dataAuthor = book.dataAuthor
        bookData.dataImageLink = book.dataImageLink
        bookData.dataImage = book.dataImage
        bookData.dataPublisher = book.dataPublisher
        bookData.dataSalesDate = book.dataSalesDate
        bookData.dataRegistDate = book.dataRegistDate
        bookData.dataItemPrice = book.dataItemPrice
        bookData.dataItemCaption = book.dataItemCaption
        bookData.dataImpression = book.dataImpression
        bookData.dataAffiliateUrl = book.dataAffiliateUrl
        bookData.dataReadStartDay = ""
        bookData.dataReadEndDay = ""
        bookData.dataSize = book.dataSize
        bookData.dataISBN = book.dataISBN
        bookData.dataLabel = book.dataLabel
        bookData.dataStatus = dataStatus
        bookData.dataRegistNumber = bookRegistNumber
        bookData.dataCollection = bookTagTextField
        let realm = try! Realm()
        try! realm.write {
            realm.add(bookData)
        }
        self.saveBookRegistNumber()
        self.continueCapture()
    }
    
    func bookAlertSheet(book: RealmBookData){
        let alertController=UIAlertController(title: "本棚に登録", message: "タグと読書状況を入力してください", preferredStyle: UIAlertController.Style.alert)
        var titleName:[String] = ["欲しい","読んでいる","未読","既読","登録をやめる"]
        alertController.addTextField{(textFieldA) -> Void in
            self.textField = textFieldA
            self.textField.delegate = self
            self.textField.placeholder = "タグを入力してください"
            self.textField.textAlignment = .center
            self.textField.isEnabled = false
            
            let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
            let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
            let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let history = UIBarButtonItem(title: "履歴", style: .plain, target: self, action: #selector(self.history))
            toolbar.setItems([doneItem,flexibleItem,history], animated: true)
            self.textField.inputAccessoryView = toolbar
            
            if let tagHistory:[String] = UserDefaults.standard.stringArray(forKey: "TagHistoryArray"){
                self.tagHistoryArray = tagHistory
            }
        }
        let defaultAction = UIAlertAction(title: titleName[0],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(book: book, dataStatus: titleName[0], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let defaultAction1 = UIAlertAction(title: titleName[1],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(book: book, dataStatus: titleName[1], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let defaultAction2 = UIAlertAction(title: titleName[2],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(book: book, dataStatus: titleName[2], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let defaultAction3 = UIAlertAction(title: titleName[3],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(book: book, dataStatus: titleName[3], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let cancel=UIAlertAction(title: titleName[4],style: UIAlertAction.Style.destructive,handler: {
            (action: UIAlertAction!) -> Void in
            //カメラで読み込みを再開する。
            self.captureSession.startRunning()
        })
        alertController.addAction(defaultAction)
        alertController.addAction(defaultAction1)
        alertController.addAction(defaultAction2)
        alertController.addAction(defaultAction3)
        alertController.addAction(cancel)
        self.present(alertController,animated: true,completion: {
            alertController.textFields?.first?.isEnabled = true
        })
    }
    
    //タグの履歴を保存する処理。
    func tagHistoryArraySave(text: String){
        if(tagHistoryArray.firstIndex(of: text) == nil && text != ""){//履歴にないデータ　かつ　未入力は入れない
            tagHistoryArray.insert(text, at: 0)
            UserDefaults.standard.set(tagHistoryArray, forKey: "TagHistoryArray")
        }
    }
}


//タグの履歴表示のためのクラス
extension BarCodeViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //表示する数
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return tagHistoryArray.count + 1
    }
    //表示するためのもの
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (row == 0) {
            return ""
        }else{
            return tagHistoryArray[row - 1]
        }
    }
    //選択された時
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row != 0 {
            textField.text = tagHistoryArray[row - 1]
        }
    }
    
    @objc func done() {
        textField.endEditing(true)
    }
    
    @objc func history(){
        //PickerViewの設定
        let pickerView: UIPickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.showsSelectionIndicator = true
        self.textField.inputView = pickerView
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let history = UIBarButtonItem(title: "キー", style: .plain, target: self, action: #selector(self.keyboard))
        toolbar.setItems([doneItem,flexibleItem,history], animated: true)
        self.textField.inputAccessoryView = toolbar
        
        self.textField.reloadInputViews()
    }
    
    @objc func keyboard() {
        //https://starzero.hatenablog.com/entry/2014/08/05/170122 普通のキーボードに戻す処理
        self.textField.inputView = nil
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let history = UIBarButtonItem(title: "履歴", style: .plain, target: self, action: #selector(self.history))
        toolbar.setItems([doneItem,flexibleItem,history], animated: true)
        self.textField.inputAccessoryView = toolbar
        
        self.textField.reloadInputViews()
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
