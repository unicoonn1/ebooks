//
//  AdNativeTableViewCell.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2020/03/01.
//  Copyright © 2020 高柳亮太. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdNativeCell: UITableViewCell{
    
    
    @IBOutlet weak var nativeAdView: GADUnifiedNativeAdView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
