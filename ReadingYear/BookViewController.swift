//
//  CellViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/05/02.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import RealmSwift

//var bookTableView:UITableView=UITableView()
//var bookRegistNumber: Int = 0 //登録順にソートするために必要な変数
//var bookToReadRegistNumber: Int = 0


class BookViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    var sortNumber = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        //遷移先から戻るの時の名前をbackから戻るに変更
        let backButtonItem = UIBarButtonItem(title: "戻る", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem
        
        bookTableView=self.tableView
        
        //読み込み処理
//        let userDefaults=UserDefaults.standard
//        if let bookListData=userDefaults.object(forKey: "bookList")as? Data{
//            if let storedBookList=NSKeyedUnarchiver.unarchiveObject(with: bookListData)as? [BookData]{
//                bookList.append(contentsOf: storedBookList)
//            }
//        }
//        if let bookToReadListData=userDefaults.object(forKey: "bookToReadList")as? Data{
//            if let storedBookToReadList=NSKeyedUnarchiver.unarchiveObject(with: bookToReadListData)as? [BookData]{
//                bookToReadList.append(contentsOf: storedBookToReadList)
//            }
//        }
        
//        let userDefaults = UserDefaults.standard
//        if let value:Int = userDefaults.object(forKey: "bookRegistNumber") as? Int{
//            bookRegistNumber = value
//        }
//        if let value:Int = userDefaults.object(forKey: "bookToReadRegistNumber") as? Int{
//            bookToReadRegistNumber = value
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //戻った時にセルの選択状態を解除する処理。
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //セルの選択状態を解除する処理。　戻った時にどのセルを選択していたかを見やすくするための処理。
        if let indexPathForSelectedRow = self.tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPathForSelectedRow, animated: true)
        }
    }
    
    @IBAction func sortButtonAction(_ sender: UIBarButtonItem) {
        let alert: UIAlertController = UIAlertController(title: "並び替え変更", message: .none, preferredStyle:  UIAlertController.Style.actionSheet)
        var titleName:[String] = ["登録順","タイトル順","著者順"]
        if(self.sortNumber == 0){
            titleName[0] = "登録順(選択中)"
            titleName[1] = "タイトル順"
            titleName[2] = "著者順"
        }else if(self.sortNumber == 1){
            titleName[0] = "登録順"
            titleName[1] = "タイトル順(選択中)"
            titleName[2] = "著者順"
        }else{
            titleName[0] = "登録順"
            titleName[1] = "タイトル順"
            titleName[2] = "著者順(選択中)"
        }
        
        let defaultAction: UIAlertAction = UIAlertAction(title: titleName[0], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.sortNumber = 0
            self.tableView.reloadData()
        })
        
        let defaultAction1: UIAlertAction = UIAlertAction(title: titleName[1], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.sortNumber = 1
            self.tableView.reloadData()
        })
        let defaultAction2: UIAlertAction = UIAlertAction(title: titleName[2], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.sortNumber = 2
            self.tableView.reloadData()
        })
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler:{
            (action: UIAlertAction!) -> Void in
        })

        alert.addAction(defaultAction)
        alert.addAction(defaultAction1)
        alert.addAction(defaultAction2)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    //section使う時に必須のメソッドっぽい
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "あ"
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label : UILabel = UILabel()
        label.backgroundColor = UIColor(red: 0, green: 160/255, blue: 1, alpha: 1)
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 14)//UIのフォントとサイズ
        if(sortNumber == 0){
            label.text = "登録順"
        }else if(sortNumber == 1){
            label.text = "タイトル順"
        }else{
            label.text = "著者順"
        }
        return label
    }

    
    //セルの個数を指定するデリゲートメソッド(必須)
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        let realm = try! Realm()
        let result = realm.objects(RealmBookData.self)
        return result.count
    }
    
    //セルに値を設定するデータソースメソッド(必須)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookCell",for: indexPath) as! BookTableViewCell
        
        let realm = try! Realm()
        var result = realm.objects(RealmBookData.self)
        if(sortNumber == 0){
            result = realm.objects(RealmBookData.self).sorted(byKeyPath: "dataRegistNumber", ascending: false)//降順 4-3-2-1-0の順
        }else if(sortNumber == 1){
            result = realm.objects(RealmBookData.self).sorted(byKeyPath: "dataTitle", ascending: true)
        }else{
            result = realm.objects(RealmBookData.self).sorted(byKeyPath: "dataAuthor", ascending: true)
        }
        let book = result[indexPath.row]
        
        cell.bookImage.image = UIImage(data: book.dataImage as Data)
        cell.bookTitle.text = book.dataTitle
        cell.bookAuthor.text = book.dataAuthor

        //セルのハイライト色をグレーから薄い青に変更する処理。
        let selectedBackgroundView: UIView = UIView()
        selectedBackgroundView.backgroundColor = UIColor(red: 0, green: 135/255, blue: 1, alpha: 0.3)
        cell.selectedBackgroundView = selectedBackgroundView
        return cell
    }
    
    //segue遷移先に値渡し
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let bookDetailView:BookDetailTableViewController = segue.destination as! BookDetailTableViewController
//        bookDetailView.hidesBottomBarWhenPushed=true //遷移後の画面でタブバーを消去
//        if let indexPath = self.tableView.indexPathForSelectedRow{
//            let realm = try! Realm()
//            var result = realm.objects(RealmBookData.self)
//            if(sortNumber == 0){
//                result = realm.objects(RealmBookData.self).sorted(byKeyPath: "dataRegistNumber", ascending: false)//降順 4-3-2-1-0の順
//            }else if(sortNumber == 1){
//                result = realm.objects(RealmBookData.self).sorted(byKeyPath: "dataTitle", ascending: true)
//            }else{
//                result = realm.objects(RealmBookData.self).sorted(byKeyPath: "dataAuthor", ascending: true)
//            }
//            let bookData = result[indexPath.row]
//            bookDetailView.bookData=bookData
//        }
//    }
    

    //セルの削除処理 スライド
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle,forRowAt indexPath: IndexPath){
        if editingStyle == .delete{
            let realm = try! Realm()
            var result = realm.objects(RealmBookData.self)
            if(sortNumber == 0){
                result = realm.objects(RealmBookData.self).sorted(byKeyPath: "dataRegistNumber", ascending: false)//降順 4-3-2-1-0の順
            }else if(sortNumber == 1){
                result = realm.objects(RealmBookData.self).sorted(byKeyPath: "dataTitle", ascending: true)
            }else{
                result = realm.objects(RealmBookData.self).sorted(byKeyPath: "dataAuthor", ascending: true)
            }
            try! realm.write {
                realm.delete(result[indexPath.row])
            }
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
