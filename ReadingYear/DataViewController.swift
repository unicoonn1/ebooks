//
//  DataViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2018/06/25.
//  Copyright © 2018年 高柳亮太. All rights reserved.
//

import UIKit
import GoogleMobileAds

class DataViewController: UIViewController,GADBannerViewDelegate {
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let color:UIColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        bannerView.backgroundColor = color
        
        // AdMobで発行された広告ユニットIDを設定
        bannerView.adUnitID = "ca-app-pub-9383479591463806/2401370749"
        //simuration用
        //bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.delegate = self
        bannerView.rootViewController = self
        let gadRequest:GADRequest = GADRequest()
        // テスト用の広告を表示する時のみ使用（申請時に削除）
        //gadRequest.testDevices = [ kGADSimulatorID ]
        bannerView.load(gadRequest)
        
        //遷移先から戻るの時の名前をbackから戻るに変更
        let backButtonItem = UIBarButtonItem(title: "戻る", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}
