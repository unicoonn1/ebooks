//
//  DataTableViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2018/05/17.
//  Copyright © 2018年 高柳亮太. All rights reserved.
//

import UIKit
import RealmSwift

class DataTableViewController: UITableViewController {

    @IBOutlet weak var bookShelfLabel: UILabel!
    @IBOutlet weak var wantLabel: UILabel!
    @IBOutlet weak var readingLabel: UILabel!
    @IBOutlet weak var unreadLabel: UILabel!
    @IBOutlet weak var alreadyreadLabel: UILabel!
    @IBOutlet weak var thisYearLabel: UILabel!
    @IBOutlet weak var lastYearLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let color:UIColor=UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        tableView.backgroundColor=color
        
        for i in 0..<2{
            let indexPath:IndexPath = IndexPath(row: i, section: 1)
            let cell:UITableViewCell = tableView(tableView, cellForRowAt: indexPath)
            let selectedBackgroundView: UIView = UIView()
            selectedBackgroundView.backgroundColor = UIColor(red: 0, green: 135/255, blue: 1, alpha: 0.3)
            cell.selectedBackgroundView = selectedBackgroundView
        }
        
        setLabel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
        
    func setLabel(){
        bookShelfLabel.text = numberOfBook(str: "本棚") + "冊"
        wantLabel.text = numberOfBook(str: "欲しい") + "冊"
        readingLabel.text = numberOfBook(str: "読んでいる") + "冊"
        unreadLabel.text = numberOfBook(str: "未読") + "冊"
        alreadyreadLabel.text = numberOfBook(str: "既読") + "冊"
        
        //今年の年度を取得
        let calendar = Calendar.current
        var year = String(calendar.component(.year, from: Date()))
        let realm = try! Realm()
        thisYearLabel.text = String(realm.objects(RealmBookData.self).filter("dataRegistDate BEGINSWITH '\(year)'").count) + "冊"
        year = String(Int(year)! - 1)
        lastYearLabel.text = String(realm.objects(RealmBookData.self).filter("dataRegistDate BEGINSWITH '\(year)'").count) + "冊"
    }
    
    //冊数を返す処理。
    func numberOfBook(str:String) -> String {
        let realm = try! Realm()
        if(str == "本棚"){
            let result = realm.objects(RealmBookData.self).count
            return String(result)
        }else{
            let result = realm.objects(RealmBookData.self).filter("dataStatus = '\(str)'").count
            return String(result)
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 5
        }else{
            return 2
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let graphVC:GraphViewController = segue.destination as! GraphViewController
        //今年の年度を取得
        let calendar = Calendar.current
        let year = String(calendar.component(.year, from: Date()))
        
        if( segue.identifier == "thisYear"){
            graphVC.text = "今年"
            graphVC.year = year            
        }else{
            graphVC.text = "昨年"
            graphVC.year = String(Int(year)! - 1)
        }
    }
    
}
