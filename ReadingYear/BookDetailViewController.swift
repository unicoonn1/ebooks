//
//  BookDetailViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/12/25.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import RealmSwift

class BookDetailViewController: UIViewController {

    @IBOutlet weak var bookDetailImage: UIImageView!
    @IBOutlet weak var bookDetailTitle: UILabel!
    @IBOutlet weak var bookDetailAuthor: UILabel!
    @IBOutlet weak var bookDetailPublisher: UILabel!
    @IBOutlet weak var bookDetailLabel: UILabel!
    @IBOutlet weak var bookDetailSize: UILabel!
    @IBOutlet weak var bookDetailSalesDate: UILabel!
    @IBOutlet weak var bookDetailItemPrice: UILabel!
    @IBOutlet weak var bookDetailISBN: UILabel!
    @IBOutlet weak var bookDetailItemCaption: UILabel!
    @IBOutlet weak var bookRegistButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var image:UIImage=UIImage()
    var detailTitle:String=""
    var author:String=""
    var publisher:String=""
    var label:String=""
    var size:String=""
    var salesDate:String=""
    var itemPrice:String=""
    var isbn:String = ""
    var itemCaption:String = ""
    var affiliateUrl:String = ""
    
    var indexPath:IndexPath = IndexPath()
    
    var countFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO: スクロールビューを必ず画面いっぱいにする処理が必要
//        let color:UIColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
//        self.view.backgroundColor = color
        
        bookDetailImage.image = image
        bookDetailTitle.text = detailTitle
        bookDetailAuthor.text = author
        bookDetailPublisher.text = "出版 : " + publisher
        bookDetailLabel.text = "レーベル : " + label
        bookDetailSize.text = "サイズ : " + size
        bookDetailSalesDate.text = "発売日 : " + salesDate
        bookDetailItemPrice.text = "値段 : " + itemPrice
        bookDetailISBN.text = "ISBN : " + isbn
        
        if(itemCaption == ""){
            bookDetailItemCaption.text = "説明なし"
        }else{
            bookDetailItemCaption.text = itemCaption + "\n\n\n"
        }
        
        bookRegistButton.imageView?.contentMode = .scaleAspectFit
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
        
    @IBAction func affiliateUrlGo(_ sender: UIBarButtonItem) {
        let alert: UIAlertController = UIAlertController(title: "外部サイトで書籍情報を調べる", message: .none, preferredStyle:  UIAlertController.Style.actionSheet)
        
        let defaultAction: UIAlertAction = UIAlertAction(title: "楽天の購入画面へ", style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            let url = URL(string: self.affiliateUrl)
            if UIApplication.shared.canOpenURL(url! as URL){
                UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        })
        let defaultAction1: UIAlertAction = UIAlertAction(title: "ブックオフの買取価格画面へ", style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            let bookOffUrl = "https://www.bookoffonline.co.jp/boleccontent/bolbuysearch/buysearch/search?bg=&q=" + self.isbn
            let url = URL(string: bookOffUrl)
            if UIApplication.shared.canOpenURL(url! as URL){
                UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        })
        let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler:{
            (action: UIAlertAction!) -> Void in
        })
        
        alert.addAction(defaultAction)
        alert.addAction(defaultAction1)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    

    @IBAction func bookShelfRegist(_ sender: UIButton) {
        let count:Int = (self.navigationController?.children.count)!//ないはずのchildにアクセスしてしまうため
        if(count == 2){
            if let popularTVC = self.navigationController?.children[0] as? PopularTableViewController{
                popularTVC.sameBookAlert(indexPath: indexPath)
            }
        }else if(count == 3){
            if let searchVC = self.navigationController?.children[1] as? SearchViewController{
                searchVC.sameBookAlert(indexPath: indexPath)
            }
        }else{
            if let detailSearchBookTVC = self.navigationController?.children[2] as? DetailSearchBookTableViewController{
                detailSearchBookTVC.sameBookAlert(indexPath: indexPath)
            }
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
