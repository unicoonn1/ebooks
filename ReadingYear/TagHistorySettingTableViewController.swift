//
//  TagHistorySettingTableViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2018/06/13.
//  Copyright © 2018年 高柳亮太. All rights reserved.
//

import UIKit

class TagHistorySettingTableViewController: UITableViewController {
    
    var tagHistoryArray:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: .zero)
        let color:UIColor=UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        tableView.backgroundColor=color
        
        if let tagHistory:[String] = UserDefaults.standard.stringArray(forKey: "TagHistoryArray"){
            tagHistoryArray = tagHistory
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tagHistoryArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "basic", for: indexPath)
        cell.textLabel?.text = tagHistoryArray[indexPath.row]
        cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        if tagHistoryArray.count == indexPath.row + 1 {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            tagHistoryArray.remove(at: indexPath.row)
            UserDefaults.standard.set(tagHistoryArray, forKey: "TagHistoryArray")
            UserDefaults.standard.synchronize()
            
            tableView.deleteRows(at: [indexPath], with: .fade)
            if(tagHistoryArray.isEmpty){
                tableView.reloadData()
            }
        }
    }
}
