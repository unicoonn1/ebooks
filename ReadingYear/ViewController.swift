//
//  ViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/09/28.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit

var bookRegistNumber: Int = 0 //登録順にソートするために必要な変数

class ViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerBookTableView: UIView!
    @IBOutlet weak var containerBookCollectionView: UIView!
    
    var bookTableViewVC:BookTableViewController!
    var bookCollectionViewVC:BookCollectionViewController!
    //trueがtableviewcontroller falseがcollectionviewcontroller
    var displayFlag:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //userdefault作ってdisplayflagがtrueかfalseかによってどっちにするかを判断する感じにすれば良さそう。
        containerView.bringSubviewToFront(containerBookTableView)
        containerBookCollectionView.isHidden = true

        //前回起動時の表示形式を引き継ぐ処理。
        let userDefaults = UserDefaults.standard
        if let viewFlag: Bool = userDefaults.object(forKey: "collectionViewCheckBool") as? Bool{
            if viewFlag == true{
                self.changeContainerView()
            }
        }
        
        if let value: Int = userDefaults.object(forKey: "bookRegistNumber") as? Int{
            bookRegistNumber = value
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //左上ボタンタップでcontainerview変更している処理。
    func changeContainerView(){
        if(displayFlag){
            bookCollectionViewVC.searchText = bookTableViewVC.searchText
            bookCollectionViewVC.filterText = bookTableViewVC.filterText
            bookCollectionViewVC.sortText = bookTableViewVC.sortText
            bookCollectionViewVC.collectionView?.reloadData()
            containerBookCollectionView.isHidden = false
            containerBookTableView.isHidden = true
            containerView.bringSubviewToFront(containerBookCollectionView)
            
            displayFlag = false
            
            let userDefaults = UserDefaults.standard
            userDefaults.set(true, forKey: "collectionViewCheckBool")
        }else{
            bookTableViewVC.searchText = bookCollectionViewVC.searchText
            bookTableViewVC.filterText = bookCollectionViewVC.filterText
            bookTableViewVC.sortText = bookCollectionViewVC.sortText
            bookTableViewVC.tableView.reloadData()
            containerBookTableView.isHidden = false
            containerBookCollectionView.isHidden = true
            containerView.bringSubviewToFront(containerBookTableView)
            
            displayFlag = true
            let userDefaults = UserDefaults.standard
            userDefaults.set(false, forKey: "collectionViewCheckBool")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "BookTableViewController"){
            if let navi = segue.destination as? UINavigationController{
                let tableVC = navi.children[0] as? BookTableViewController
                bookTableViewVC = tableVC
            }
        }else if(segue.identifier == "BookCollectionViewController"){
            if let navi = segue.destination as? UINavigationController{
                let collectionVC = navi.children[0] as? BookCollectionViewController
                bookCollectionViewVC = collectionVC
            }
        }
    }
}
