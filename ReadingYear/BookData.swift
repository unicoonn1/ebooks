//
//  BookData.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/05/02.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import Foundation
import UIKit

class BookData:NSObject, NSCoding{
    
    var dataRegistNumber:Int = 0 //登録した順にソートをさせるための変数　本棚や読みたい本に追加した順を記憶。
    var dataReadStartDay:String? = ""//読み始めた日
    var dataReadEndDay:String? = ""//読み終えた日
    var dataSize:String? = ""
    var dataISBN:String? = ""
    var dataLabel:String? = ""
    
    
    //データ保存
    var dataTitle:String?="" //タイトル
    var dataAuthor:String?="" //著者
    var dataImageLink:String?="" //イメージリンク
    var dataImage:UIImage?=UIImage() //イメージ
    var dataPublisher:String?="" //出版社
    var dataSalesDate:String?="" //発売日
    var dataRegistDate:String?="" //登録日
    var dataItemPrice:String?="" //値段
    var dataItemCaption:String?="" //あらすじ
    var dataImpression:String?="" //感想
    var dataAffiliateUrl:String?="" //楽天URL
    
    override init(){
        
    }
    
    required init?(coder aDecoder: NSCoder){
        dataTitle=aDecoder.decodeObject(forKey: "dataTitle") as? String
        dataAuthor=aDecoder.decodeObject(forKey: "dataAuthor") as? String
        dataImage=aDecoder.decodeObject(forKey: "dataImage") as? UIImage
        dataPublisher=aDecoder.decodeObject(forKey: "dataPublisher") as? String
        dataSalesDate=aDecoder.decodeObject(forKey: "dataSalesDate") as? String
        dataRegistDate=aDecoder.decodeObject(forKey: "dataRegistDate") as? String
        dataItemPrice=aDecoder.decodeObject(forKey: "dataItemPrice") as? String
        dataItemCaption=aDecoder.decodeObject(forKey: "dataItemCaption") as? String
        dataImpression=aDecoder.decodeObject(forKey: "dataImpression") as? String
        dataAffiliateUrl=aDecoder.decodeObject(forKey: "dataAffiliateUrl") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(dataTitle,forKey: "dataTitle")
        aCoder.encode(dataAuthor,forKey: "dataAuthor")
        aCoder.encode(dataImage,forKey: "dataImage")
        aCoder.encode(dataPublisher,forKey: "dataPublisher")
        aCoder.encode(dataSalesDate,forKey: "dataSalesDate")
        aCoder.encode(dataRegistDate,forKey: "dataRegistDate")
        aCoder.encode(dataItemPrice,forKey: "dataItemPrice")
        aCoder.encode(dataItemCaption,forKey: "dataItemCaption")
        aCoder.encode(dataImpression,forKey: "dataImpression")
        aCoder.encode(dataAffiliateUrl,forKey: "dataAffiliateUrl")
    }
}
