//
//  BookShelfDetailViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/12/28.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import RealmSwift

//このクラスは処理が汚い
class BookShelfDetailViewController: UIViewController,UITextFieldDelegate{

    
    @IBOutlet weak var bookDetailImage: UIImageView!
    @IBOutlet weak var bookDetailTitle: UILabel!
    @IBOutlet weak var bookDetailAuthor: UILabel!
    @IBOutlet weak var bookDetailPublisher: UILabel!
    @IBOutlet weak var bookDetailLabel: UILabel!
    @IBOutlet weak var bookDetailSize: UILabel!
    @IBOutlet weak var bookDetailSalesDate: UILabel!
    @IBOutlet weak var bookDetailItemPrice: UILabel!
    @IBOutlet weak var bookDetailItemCaption: UILabel!
    @IBOutlet weak var bookDetailImpression: UILabel!
    @IBOutlet weak var bookDetailISBN: UILabel!
    
    @IBOutlet weak var bookDetailRegistDate: UIButton!
    @IBOutlet weak var bookDetailReadStartDay: UIButton!
    @IBOutlet weak var bookDetailReadEndDay: UIButton!
    @IBOutlet weak var bookStatus: UIImageView!
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secoundButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    @IBOutlet weak var fourthButton: UIButton!
    @IBOutlet weak var fifthButton: UIButton!
    
    @IBOutlet weak var bookRegistButton: UIButton!
    
    
    var bookData:RealmBookData! //絶対値が入る
    var selectNumber:Int = 0
    
    
    //tagのためのtextField
    var textField: UITextField!
    //このリストにタグの履歴のデータを入れる。
    var tagHistoryArray:[String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let color:UIColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
//        self.view.backgroundColor = color
        
        setCellData(book: bookData)
        //レイアウトがガクガクなるためこっちに持ってきた
        if(bookData.dataItemCaption  == ""){
            bookDetailItemCaption.text = "説明なし"
        }else{
            bookDetailItemCaption.text = bookData.dataItemCaption + "\n\n\n"
        }
        
        //必ず説明欄を１０行以上にする処理。
        bookDetailItemCaption.sizeToFit()
        let count = Int(bookDetailItemCaption.frame.height / bookDetailItemCaption.font.lineHeight)
        if count < 10{
            for _ in 0 ..< 10 - count{
                bookDetailItemCaption.text = bookDetailItemCaption.text! + "\n"
            }
        }
        
        bookRegistButton.imageView?.contentMode = .scaleAspectFit
        
        //遷移先から戻るの時の名前をbackから戻るに変更
        let backButtonItem = UIBarButtonItem(title: "戻る", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func setCellData(book: RealmBookData){
        bookDetailImage.image = UIImage(data: book.dataImage as Data)
        bookDetailTitle.text = book.dataTitle
        bookDetailAuthor.text = book.dataAuthor
        bookDetailPublisher.text = "出版 : " + book.dataPublisher
        bookDetailLabel.text = "レーベル : " + book.dataLabel
        bookDetailSize.text = "サイズ : " + book.dataSize
        bookDetailSalesDate.text = "発売日 : " + book.dataSalesDate
        bookDetailItemPrice.text = "値段 : " + book.dataItemPrice
        bookDetailISBN.text = "ISBN : " + book.dataISBN
        if(book.dataImpression == ""){
            bookDetailImpression.text = "\n\n\n\n\n\n\n"
        }else{
            bookDetailImpression.text = book.dataImpression + "\n\n\n\n\n\n\n"
        }
        
        bookDetailRegistDate.titleLabel!.lineBreakMode = .byWordWrapping
        bookDetailRegistDate.titleLabel!.numberOfLines = 2
        bookDetailRegistDate.titleLabel!.textAlignment = .center
        let arr:[String] = book.dataRegistDate.components(separatedBy: "/")//年月日を付与するための処理。
        let registDate = arr[0]+"年"+arr[1]+"月"+arr[2]+"日"
        bookDetailRegistDate.setTitle("登録日\n" + registDate, for: .normal)
        
        bookDetailReadStartDay.titleLabel!.lineBreakMode = .byWordWrapping
        bookDetailReadStartDay.titleLabel!.numberOfLines = 2
        bookDetailReadStartDay.titleLabel!.textAlignment = .center
        if(book.dataReadStartDay == ""){
            bookDetailReadStartDay.setTitle("読み始めた日\n未設定", for: .normal)
        }else{
            bookDetailReadStartDay.setTitle("読み始めた日\n" + book.dataReadStartDay, for: .normal)
        }
        
        bookDetailReadEndDay.titleLabel!.lineBreakMode = .byWordWrapping
        bookDetailReadEndDay.titleLabel!.numberOfLines = 2
        bookDetailReadEndDay.titleLabel!.textAlignment = .center
        if(book.dataReadEndDay == ""){
            bookDetailReadEndDay.setTitle("読み終えた日\n未設定", for: .normal)
        }else{
            bookDetailReadEndDay.setTitle("読み終えた日\n" + book.dataReadEndDay, for: .normal)
        }
        
        //ボタンのタイトルと写真設定を行なっている。
        if(bookData.dataStatus == "既読"){
            bookStatus.image = UIImage()
        }else if(bookData.dataStatus == "未読"){
            bookStatus.image = UIImage(named: "BookStack.png")
        }else if(bookData.dataStatus == "欲しい"){
            bookStatus.image = UIImage(named: "Star.png")
        }else{
            bookStatus.image = UIImage(named: "BookRead.png")
        }
        
        
        if(bookData.dataEvaluation == 0){
            firstButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
            secoundButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
            thirdButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
            fourthButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
            fifthButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
        }else if(bookData.dataEvaluation == 1){
            firstButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            secoundButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
            thirdButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
            fourthButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
            fifthButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
        }else if(bookData.dataEvaluation == 2){
            firstButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            secoundButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            thirdButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
            fourthButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
            fifthButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
        }else if(bookData.dataEvaluation == 3){
            firstButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            secoundButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            thirdButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            fourthButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
            fifthButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
        }else if(bookData.dataEvaluation == 4){
            firstButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            secoundButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            thirdButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            fourthButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            fifthButton.setImage(#imageLiteral(resourceName: "GrayStar"), for: .normal)
        }else{
            firstButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            secoundButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            thirdButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            fourthButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
            fifthButton.setImage(#imageLiteral(resourceName: "OrangeStar"), for: .normal)
        }
        
        
        //感想ラベルをタップで画面遷移させるために必要な処理
        bookDetailImpression.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.impressionTap))
        bookDetailImpression.addGestureRecognizer(tapGesture)
    }
    
    @IBAction func readSituation(_ sender: UIButton) {
        let alert: UIAlertController = UIAlertController(title: "本を編集", message: "タグと読書状況を編集してください", preferredStyle:  UIAlertController.Style.alert)
        var titleName:[String] = ["欲しい","読んでいる","未読","既読"]
        let realm = try! Realm()
        alert.addTextField{(textFieldA) -> Void in
            self.textField = textFieldA
            self.textField.delegate = self
            self.textField.text = self.bookData.dataCollection
            self.textField.placeholder = "タグを入力してください"
            self.textField.textAlignment = .center
            self.textField.isEnabled = false
            
            let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
            let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
            let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let history = UIBarButtonItem(title: "履歴", style: .plain, target: self, action: #selector(self.history))
            toolbar.setItems([doneItem,flexibleItem,history], animated: true)
            self.textField.inputAccessoryView = toolbar
            
            if let tagHistory:[String] = UserDefaults.standard.stringArray(forKey: "TagHistoryArray"){
                self.tagHistoryArray = tagHistory
            }
        }
        
        let defaultAction: UIAlertAction = UIAlertAction(title: titleName[0], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            try! realm.write {
                self.bookData.setValue(titleName[0], forKey: "dataStatus")
                self.bookData.setValue((alert.textFields?.first?.text)!, forKey: "dataCollection")
            }
            self.tagHistoryArraySave(text: (alert.textFields?.first?.text)!)
            bookTableView.reloadData()
            self.setCellData(book: self.bookData)//image書き換えのためだけに呼んでいる
        })
        let defaultAction1: UIAlertAction = UIAlertAction(title: titleName[1], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            try! realm.write {
                self.bookData.setValue(titleName[1], forKey: "dataStatus")
                self.bookData.setValue((alert.textFields?.first?.text)!, forKey: "dataCollection")
            }
            self.tagHistoryArraySave(text: (alert.textFields?.first?.text)!)
            bookTableView.reloadData()
            self.setCellData(book: self.bookData)
        })
        let defaultAction2: UIAlertAction = UIAlertAction(title: titleName[2], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            try! realm.write {
                self.bookData.setValue(titleName[2], forKey: "dataStatus")
                self.bookData.setValue((alert.textFields?.first?.text)!, forKey: "dataCollection")
            }
            self.tagHistoryArraySave(text: (alert.textFields?.first?.text)!)
            bookTableView.reloadData()
            self.setCellData(book: self.bookData)
        })
        let defaultAction3: UIAlertAction = UIAlertAction(title: titleName[3], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            try! realm.write {
                self.bookData.setValue(titleName[3], forKey: "dataStatus")
                self.bookData.setValue((alert.textFields?.first?.text)!, forKey: "dataCollection")
            }
            self.tagHistoryArraySave(text: (alert.textFields?.first?.text)!)
            bookTableView.reloadData()
            self.setCellData(book: self.bookData)
        })
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler:{
            (action: UIAlertAction!) -> Void in
            bookTableView.reloadData()
        })
        
        if(bookData.dataStatus == titleName[0]){
            defaultAction.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction.setValue(UIColor.orange, forKey: "titleTextColor")
        }else if(bookData.dataStatus == titleName[1]){
            defaultAction1.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction1.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction1.setValue(UIColor.orange, forKey: "titleTextColor")
        }else if(bookData.dataStatus == titleName[2]){
            defaultAction2.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction2.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction2.setValue(UIColor.orange, forKey: "titleTextColor")
        }else{
            defaultAction3.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction3.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction3.setValue(UIColor.orange, forKey: "titleTextColor")
        }
        
        alert.addAction(defaultAction)
        alert.addAction(defaultAction1)
        alert.addAction(defaultAction2)
        alert.addAction(defaultAction3)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: {
            alert.textFields?.first?.isEnabled = true
        })
    }
    
    @IBAction func bookDeleteButton(_ sender: UIBarButtonItem) {
        let alert: UIAlertController = UIAlertController(title: "削除", message: "本当に削除してよろしいですか？", preferredStyle:  UIAlertController.Style.alert)
        let defaultAction: UIAlertAction = UIAlertAction(title: "削除", style: UIAlertAction.Style.destructive, handler:{
            (action: UIAlertAction!) -> Void in
            if let vc = self.navigationController?.children[0] as? BookTableViewController {
                vc.bookDeleteFlag = true
            }
            if let vc = self.navigationController?.children[0] as? BookCollectionViewController {
                vc.bookDeleteFlag = true
            }
            
            self.navigationController?.popViewController(animated: true)
        })
        let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(defaultAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func affiliateUrlGo(_ sender: UIBarButtonItem) {
        let alert: UIAlertController = UIAlertController(title: "外部サイトで書籍情報を調べる", message: .none, preferredStyle:  UIAlertController.Style.actionSheet)
        
        let defaultAction: UIAlertAction = UIAlertAction(title: "楽天の購入画面へ", style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            let url = URL(string: self.bookData.dataAffiliateUrl)
            if UIApplication.shared.canOpenURL(url! as URL){
                UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        })
        let defaultAction1: UIAlertAction = UIAlertAction(title: "ブックオフの買取価格画面へ", style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            let isbn:String = self.bookData.dataISBN
            let bookOffUrl:String = "https://www.bookoffonline.co.jp/boleccontent/bolbuysearch/buysearch/search?bg=&q=" + isbn
            let url = URL(string: bookOffUrl)
            if UIApplication.shared.canOpenURL(url! as URL){
                UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        })
        let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler:{
            (action: UIAlertAction!) -> Void in
        })
        
        alert.addAction(defaultAction)
        alert.addAction(defaultAction1)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //タグの履歴を保存する処理。
    func tagHistoryArraySave(text: String){
        if(tagHistoryArray.firstIndex(of: text) == nil && text != ""){//履歴にないデータ　かつ　未入力は入れない
            tagHistoryArray.insert(text, at: 0)
            UserDefaults.standard.set(tagHistoryArray, forKey: "TagHistoryArray")
        }
    }
    
    @IBAction func bookDetailRegistDay(_ sender: UIButton) {
        selectNumber = 0
        self.performSegue(withIdentifier: "ChangeDateViewController", sender: nil)
    }
    
    @IBAction func bookDetailStartDay(_ sender: UIButton) {
        selectNumber = 1
        self.performSegue(withIdentifier: "ChangeDateViewController", sender: nil)
    }
    
    @IBAction func bookDetailEndDay(_ sender: UIButton) {
        selectNumber = 2
        self.performSegue(withIdentifier: "ChangeDateViewController", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if( segue.identifier == "ChangeTextViewController"){
            let textView:ChangeTextViewController = segue.destination as! ChangeTextViewController
            textView.bookData = bookData
            textView.frontVC = self
        }else if(segue.identifier == "ChangeDateViewController"){
            let textView:ChangeDateViewController = segue.destination as! ChangeDateViewController
            if(selectNumber == 0){//登録日0
                textView.cellNumberSelect = selectNumber
            }else if(selectNumber == 1){//読み始めた日1
                textView.cellNumberSelect = selectNumber
            }else if(selectNumber == 2){//読み終えた日2
                textView.cellNumberSelect = selectNumber
            }
            textView.bookData = bookData
            textView.frontVC = self
        }
    }
    
    @objc func impressionTap() {
        self.performSegue(withIdentifier: "ChangeTextViewController", sender: nil)
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("999\n")
    }
    
    //評価の設定ができるようにする
    @IBAction func evaluationLeft(_ sender: UIButton) {
        evaluationSetting(value: 0)
    }
    @IBAction func evaluationFirst(_ sender: UIButton) {
        evaluationSetting(value: 1)
    }
    @IBAction func evaluationSecound(_ sender: UIButton) {
        evaluationSetting(value: 2)
    }
    @IBAction func evaluationThird(_ sender: UIButton) {
        evaluationSetting(value: 3)
    }
    @IBAction func evaluationFourth(_ sender: UIButton) {
        evaluationSetting(value: 4)
    }
    @IBAction func evaluationFifth(_ sender: UIButton) {
        evaluationSetting(value: 5)
    }
    @IBAction func evaluationRight(_ sender: UIButton) {
        evaluationSetting(value: 5)
    }
    
    func evaluationSetting(value: Int){
        let realm = try! Realm()
        try! realm.write {
            self.bookData.setValue(value, forKey: "dataEvaluation")
        }
        self.setCellData(book: self.bookData)
    }
}


//タグの履歴表示のためのクラス
extension BookShelfDetailViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //表示する数
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return tagHistoryArray.count + 1
    }
    //表示するためのもの
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (row == 0) {
            return ""
        }else{
            return tagHistoryArray[row - 1]
        }
    }
    //選択された時
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row != 0 {
            textField.text = tagHistoryArray[row - 1]
        }
    }
    
    @objc func done() {
        textField.endEditing(true)
    }
    
    @objc func history(){
        //PickerViewの設定
        let pickerView: UIPickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.showsSelectionIndicator = true
        self.textField.inputView = pickerView
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let history = UIBarButtonItem(title: "キー", style: .plain, target: self, action: #selector(self.keyboard))
        toolbar.setItems([doneItem,flexibleItem,history], animated: true)
        self.textField.inputAccessoryView = toolbar
        
        self.textField.reloadInputViews()
    }
    
    @objc func keyboard() {
        //https://starzero.hatenablog.com/entry/2014/08/05/170122 普通のキーボードに戻す処理
        self.textField.inputView = nil
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let history = UIBarButtonItem(title: "履歴", style: .plain, target: self, action: #selector(self.history))
        toolbar.setItems([doneItem,flexibleItem,history], animated: true)
        self.textField.inputAccessoryView = toolbar
        
        self.textField.reloadInputViews()
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
