//
//  TabBarController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/05/03.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController{

    var bookShelfTVFlag:Bool = true
    var bookShelfCVFlag:Bool = true
    var bookFlag:Bool = true
    var popularFlag:Bool = false
    var searchFlag:Bool = false
    
    var chartViewFlag:Bool = false
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //本棚のデータを年度ごとに分割
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if(item.tag == 0){
            if let VC = self.children[0] as? ViewController{
                if let navi = VC.children[0] as? UINavigationController{
                    let count = navi.children.count
                    if(bookFlag == true){
                        if(count == 1){
                            if(VC.displayFlag){//trueだとtableView falseだとcollectionView
                                if(bookShelfTVFlag){
                                    let point = CGPoint(x: 0, y: -VC.bookTableViewVC.tableView.adjustedContentInset.top)//searchbar scroll
                                    VC.bookTableViewVC.tableView.setContentOffset(point, animated: true)
                                }
                            }
                        }else if(count == 2 || count == 3){
                            navi.popViewController(animated: true)
                        }
                    }
                }
                if let naviC = VC.children[1] as? UINavigationController{
                    let count = naviC.children.count
                    if(bookFlag == true){
                        if(count == 1){
                            if(VC.displayFlag == false){//trueだとtableView falseだとcollectionView
                                if(bookShelfCVFlag){
                                    let point = CGPoint(x: 0, y: -CGFloat((VC.bookCollectionViewVC.collectionView?.adjustedContentInset.top)!))
                                    VC.bookCollectionViewVC.collectionView?.setContentOffset(point, animated: true)
                                }
                            }
                        }else if(count == 2 || count == 3){
                            naviC.popViewController(animated: true)
                        }
                    }
                }
            }
            bookFlag = true
            bookShelfTVFlag = true
            bookShelfCVFlag = true
        }else{//他のタブバーを押した際に動く処理。
            bookFlag = false
            bookShelfTVFlag = false
            bookShelfCVFlag = false
        }
        
        if(item.tag == 1){
            if let navi = self.children[1] as? UINavigationController{
                if(navi.children.count == 1){
                    if let popularTVC = navi.children[0] as? PopularTableViewController{
                        if(popularFlag){
                            if #available(iOS 11.0, *) {
                                let point = CGPoint(x: 0, y: -popularTVC.tableView.adjustedContentInset.top)
                                popularTVC.tableView.setContentOffset(point, animated: true)
                            }else{
                                let point = CGPoint(x: 0, y: -popularTVC.tableView.contentInset.top)
                                popularTVC.tableView.setContentOffset(point, animated: true)
                            }
                        }
                    }
                }
            }
            popularFlag = true
        }else{
            popularFlag = false
        }
        
            
        if(item.tag == 2){
            if(searchFlag){
                if let VC = self.children[2] as? BookSearchContainerViewController{
                    if let navi = VC.children[0] as? UINavigationController{
                        if(navi.children.count != 1){
                            navi.popViewController(animated: true)
                        }
                    }
                }
            }
            searchFlag = true
        }else{
            searchFlag = false
        }

        //リロードさせるための処理
        if(item.tag == 3){
            if let navi = self.children[3] as? UINavigationController{
                if let vc = navi.children[0] as? DataTableViewController{
                    if(chartViewFlag){
                        vc.setLabel()
                    }else{
                        chartViewFlag = true
                    }
                }
            }
        }
    }

}
