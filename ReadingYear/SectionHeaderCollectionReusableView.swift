//
//  SectionHeaderCollectionReusableView.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2018/06/03.
//  Copyright © 2018年 高柳亮太. All rights reserved.
//

import UIKit

class SectionHeaderCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var sectionLabel: UILabel!
}
