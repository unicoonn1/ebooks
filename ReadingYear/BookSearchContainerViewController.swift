//
//  BookSearchContainerViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/11/30.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit

class BookSearchContainerViewController: UIViewController {

    var bookSearchVC:BookSearchViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navi = segue.destination as? UINavigationController{
            let searchVC = navi.children[0] as? BookSearchViewController
            bookSearchVC = searchVC
        }
    }
}
