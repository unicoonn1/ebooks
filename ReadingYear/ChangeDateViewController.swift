//
//  ChangeDateViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/05/21.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import RealmSwift


class ChangeDateViewController: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    var frontVC:BookShelfDetailViewController!
    var bookData:RealmBookData=RealmBookData()
    var cellNumberSelect: Int = 0
    
    //cellの位置が変わったらここを直せばいい。
    enum cellNumber: Int {
        case dataRegistDate = 0 //登録日
        case dataReadStartDay = 1//読み始めた日
        case dataReadEndDay = 2//読み終えた日
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(cellNumberSelect == cellNumber.dataRegistDate.rawValue){
            self.navigationItem.title = "登録日"
        }else if(cellNumberSelect == cellNumber.dataReadStartDay.rawValue){
            self.navigationItem.title = "読み始めた日"
        }else{
            self.navigationItem.title = "読み終えた日"
        }
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "ja_JP") as Locale? // ロケールの設定
        dateFormatter.dateFormat = "yyyy年MM月dd日" // 日付フォーマットの設定
        if(cellNumberSelect == cellNumber.dataRegistDate.rawValue){
            let arr:[String] = bookData.dataRegistDate.components(separatedBy: "/")//年月日を付与するための処理。
            let registDate = arr[0]+"年"+arr[1]+"月"+arr[2]+"日"
            if let date = dateFormatter.date(from: registDate){
                datePicker.date = date
            }
        }else if(cellNumberSelect == cellNumber.dataReadStartDay.rawValue){
            if let date = dateFormatter.date(from: bookData.dataReadStartDay){
                datePicker.date = date
            }
        }else {
            if let date = dateFormatter.date(from: bookData.dataReadEndDay){
                datePicker.date = date
            }
        }
        //取得できなかったら今日の日付が表示される。
        
        self.datePicker.backgroundColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveButton(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "", message: "保存しました", preferredStyle: .alert)
        self.present(alert, animated: true, completion: {// アラートを閉じる
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        })
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "ja_JP") as Locale? // ロケールの設定
        dateFormatter.dateFormat = "yyyy/MM/dd" // 日付フォーマットの設定
        let date=dateFormatter.string(from: datePicker.date) //登録日
        
        if(cellNumberSelect == cellNumber.dataRegistDate.rawValue){
            let realm = try! Realm()
            try! realm.write {
                bookData.setValue(date, forKey: "dataRegistDate")
            }
        }else if(cellNumberSelect == cellNumber.dataReadStartDay.rawValue){
            let arr:[String] = date.components(separatedBy: "/")//年月日を付与するための処理。
            let realm = try! Realm()
            try! realm.write {
                bookData.setValue(arr[0]+"年"+arr[1]+"月"+arr[2]+"日", forKey: "dataReadStartDay")
            }
        }else {
            let arr:[String] = date.components(separatedBy: "/")//年月日を付与するための処理。
            let realm = try! Realm()
            try! realm.write {
                bookData.setValue(arr[0]+"年"+arr[1]+"月"+arr[2]+"日", forKey: "dataReadEndDay")
            }
        }
        bookTableView.reloadData()
        frontVC.setCellData(book: bookData)
    }
}
