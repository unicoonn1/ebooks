//
//  SearchViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/05/12.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import RealmSwift
import SwipeCellKit
import GoogleMobileAds

class SearchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,SwipeTableViewCellDelegate,GADUnifiedNativeAdLoaderDelegate{
    
    //APIのURL
    var entryUrl: String = "https://app.rakuten.co.jp/services/api/BooksBook/Search/20170404?format=json&title="
    let applicationIDForSort = "&outOfStockFlag=1&affiliateId=15be4ccf.cc97e6e6.15be4cd0.efe26c07&applicationId=1014949399335024209"//sortのために用いた
    var aplicationID:String = "&outOfStockFlag=1&affiliateId=15be4ccf.cc97e6e6.15be4cd0.efe26c07&applicationId=1014949399335024209"
    var searchBookList = [BookData]()
    var reloadUrl=""
    
    var bookPage:Int = 0
    var bookPageCount:Int = 0
    var bookCount:Int = 0
    var rock:Int = 0
    var alertRock:Int = 0
    var searchBarText = ""
    
    var indicator: UIActivityIndicatorView!
    var footerView: UIView!
    
    var searchBar: UISearchBar = UISearchBar()
    
    var sortNumber = 0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    var frontVC:BookSearchViewController!
    
    
    //tagのためのtextField
    var textField: UITextField!
    //このリストにタグの履歴のデータを入れる。
    var tagHistoryArray:[String] = []
    
    //広告用
    var adLoader: GADAdLoader!
    var nativeAdData: [GADUnifiedNativeAd] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let option = GADNativeAdMediaAdLoaderOptions()
        option.mediaAspectRatio = .portrait
        //本番
        let adID: String = "ca-app-pub-9383479591463806/8078648188"
        //テスト
        //        let adID: String = "ca-app-pub-3940256099942544/3986624511"
        adLoader = GADAdLoader(adUnitID: adID,
                               rootViewController: self,
                               adTypes: [GADAdLoaderAdType.unifiedNative],
                               options: [option])
        adLoader.delegate = self
        //        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ "2077ef9a63d2b398840261c8221a0c9b" ]
        adLoader.load(GADRequest())
        
        //カスタムセルを使う際に必須の処理
        self.tableView.register(UINib(nibName: "AdNativeCell", bundle: nil), forCellReuseIdentifier: "AdNativeCell")
                
        //遷移先から戻るの時の名前をbackから戻るに変更
        let backButtonItem = UIBarButtonItem(title: "戻る", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem
        
        let color:UIColor=UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        tableView.backgroundColor=color
        tableView.tableFooterView = UIView()
        
        //bookSearchViewControllerからの遷移での処理。文字が渡ってきてそれを扱い検索。
        searchBar.text = searchBarText
        self.searchBarSearchButtonClicked(searchBar)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //広告の処理
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
        nativeAdData.append(nativeAd)
        tableView.reloadData()
    }
    
    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        // The adLoader has finished loading ads, and a new request can be sent.
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
        print(error);
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //セルの選択状態を解除する処理。　戻った時にどのセルを選択していたかを見やすくするための処理。
        if let indexPathForSelectedRow = self.tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPathForSelectedRow, animated: true)
        }
    }
    
    func saveBookRegistNumber(){
        bookRegistNumber += 1
        let userDefaults = UserDefaults.standard
        userDefaults.set(bookRegistNumber, forKey: "bookRegistNumber")
    }
    
    //http://qiita.com/ponkichi4/items/bbb8b2bc089170f0bcb0
    //searchbarを扱いキーボードを表示させた後、searchBar以外をタップでキーボードを下げるための処理。
    override func viewDidAppear(_ animated: Bool) {
        if(searchBar.isFirstResponder){
            // ジェスチャー追加：画面タップでキーボードを閉じる
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard(_:)))
            self.view.addGestureRecognizer(tapGesture)
            // 後述のイベント切り分け用
            tapGesture.delegate = self
        }else{
            frontVC.tableView.reloadData()//前の画面のtableviewリロード 此処の書くのよくなさそう
        }
    }
    // キーボードを閉じる
    @objc func closeKeyboard(_ sender: UITapGestureRecognizer){
        searchBar.resignFirstResponder()
        searchBar.text?.removeAll()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        // キーボード表示中のみ、タップジェスチャーを実行する
        // 表示中でないなら、SubViewにイベントを移譲
        if (searchBar.isFirstResponder) {
            return true
        } else {
            return false
        }
    }
    
    // フォーカスが当たる際に呼び出されるメソッド(編集の可否を定義可能).
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    // キャンセルボタンタップ時に呼び出されるメソッド.
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text?.removeAll()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    
    @IBAction func sortButton(_ sender: UIBarButtonItem) {
        //アラートダイアログ生成
        let alertController=UIAlertController(title: "並び替え変更", message: .none, preferredStyle: UIAlertController.Style.actionSheet)
        //標準順
        let defaultAction = UIAlertAction(title:"標準順",style: UIAlertAction.Style.default){
            (action:UIAlertAction) -> Void in
            self.aplicationID = "&sort=standard" + self.applicationIDForSort
            self.searchBarSearchButtonClicked(self.searchBar)
            self.sortNumber = 0
        }
        //人気順
        let defaultAction1 = UIAlertAction(title: "人気順", style: UIAlertAction.Style.default){
            (action:UIAlertAction) -> Void in
            self.aplicationID = "&sort=sales" + self.applicationIDForSort
            self.searchBarSearchButtonClicked(self.searchBar)
            self.sortNumber = 1
        }
        //発売日が新しい順
        let defaultAction2 = UIAlertAction(title:"発売日が新しい順",style: UIAlertAction.Style.default){
            (action:UIAlertAction) -> Void in
            self.aplicationID = "&sort=-releaseDate" + self.applicationIDForSort
            self.searchBarSearchButtonClicked(self.searchBar)
            self.sortNumber = 2
        }
        //発売日が古い順
        let defaultAction3 = UIAlertAction(title: "発売日が古い順", style: UIAlertAction.Style.default){
            (action:UIAlertAction) -> Void in
            self.aplicationID = "&sort=%2BreleaseDate" + self.applicationIDForSort
            self.searchBarSearchButtonClicked(self.searchBar)
            self.sortNumber = 3
        }
        //価格が高い順
        let defaultAction4 = UIAlertAction(title:"価格が高い順",style: UIAlertAction.Style.default){
            (action:UIAlertAction) -> Void in
            self.aplicationID = "&sort=-itemPrice" + self.applicationIDForSort
            self.searchBarSearchButtonClicked(self.searchBar)
            self.sortNumber = 4
        }
        //価格が安い順
        let defaultAction5 = UIAlertAction(title: "価格が安い順", style: UIAlertAction.Style.default){
            (action:UIAlertAction) -> Void in
            self.aplicationID = "&sort=%2BitemPrice" + self.applicationIDForSort
            self.searchBarSearchButtonClicked(self.searchBar)
            self.sortNumber = 5
        }
        
        
        //写真の参考
        //https://medium.com/@maximbilan/ios-uialertcontroller-customization-5cfd88140db8
        //タイトルの色変更参考
        //http://blog.csdn.net/cjxbshowzhouyujuan/article/details/53261491
        
        if(self.sortNumber == 0){
            defaultAction.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction.setValue(UIColor.orange, forKey: "titleTextColor")
        }else if(self.sortNumber == 1){
            defaultAction1.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction1.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction1.setValue(UIColor.orange, forKey: "titleTextColor")
        }else if(self.sortNumber == 2){
            defaultAction2.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction2.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction2.setValue(UIColor.orange, forKey: "titleTextColor")
        }else if(self.sortNumber == 3){
            defaultAction3.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction3.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction3.setValue(UIColor.orange, forKey: "titleTextColor")
        }else if(self.sortNumber == 4){
            defaultAction4.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction4.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction4.setValue(UIColor.orange, forKey: "titleTextColor")
        }else{
            defaultAction5.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction5.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction5.setValue(UIColor.orange, forKey: "titleTextColor")
        }
        
        alertController.addAction(defaultAction)
        alertController.addAction(defaultAction1)
        alertController.addAction(defaultAction2)
        alertController.addAction(defaultAction3)
        alertController.addAction(defaultAction4)
        alertController.addAction(defaultAction5)
        let cancelAction=UIAlertAction(title:"CANCEL",style: UIAlertAction.Style.cancel,handler: nil)
        alertController.addAction(cancelAction)
        present(alertController,animated: true,completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        guard let inputText = searchBar.text else{
            return
        }
        
        guard inputText.lengthOfBytes(using: String.Encoding.utf8) > 0 else{
            return
        }
        
        activityView.startAnimating()
        activityView.isHidden = false//クルクル回し開始、検索中
        searchBookList.removeAll()
        self.tableView.reloadData()
        
        //初回のリクエスト
        var requestUrl = entryUrl + inputText
        requestUrl = requestUrl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        requestUrl = requestUrl + aplicationID
        
        //2回目以降のリクエスト用の処理。urlQueryAllowedは日本語をutf-8に変換している。
        reloadUrl = entryUrl+inputText
        reloadUrl = reloadUrl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        
        alertRock=1
        //APIをリクエストする
        request(requestUrl:requestUrl)
        
        //footerviewにindicatorをaddsubviewし、ぐるぐる表示をしている。ここに書いているのはあまり良くない気がする。
        footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 60))
        indicator = UIActivityIndicatorView(style: .white)
        indicator.color = UIColor.darkGray
        indicator.center = CGPoint(x: self.view.bounds.width/2, y: 30)
        footerView.addSubview(self.indicator)
        tableView.tableFooterView = footerView
        
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func request(requestUrl: String){
        //URL生成
        guard let url=URL(string: requestUrl) else{
            return
        }
        //リクエスト生成
        let request=URLRequest(url: url)
        let session=URLSession.shared
        let task=session.dataTask(with: request){(data:Data?,response:URLResponse?,error:Error?) in
            //通信完了後の処理
            //エラーチェック
            guard error==nil else{
                //エラー処理かく
                self.errorAlert(title: "エラー", message: "データがありません。")
                return
            }
            
            //JSONで返却されたデータをパースして格納する
            guard let data=data else{
                //データなし
                self.errorAlert(title: "データ無し", message: "データがありません。")
                return
            }
            
            //JSON形式への変換処理
            guard let jsonData = try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any] else{
                //変換失敗
                self.errorAlert(title: "変換失敗", message: "変換処理が出来ません。")
                return
            }
            
            //データ解析
            guard let resultSet=jsonData["Items"] as? [[String:Any]] else{
                //データなし
                self.errorAlert(title: "データ無し", message: "データがありません。")
                return
            }
            
            guard let page=jsonData["page"] as? Int else{
                return
            }
            self.bookPage=page
            guard let pageCount=jsonData["pageCount"] as? Int else{
                return
            }
            self.bookPageCount=pageCount
            guard let count=jsonData["count"] as? Int else{
                return
            }
            self.bookCount=count
            
            if(self.alertRock==1&&self.bookCount==0){//0冊だった時の処理。
                let alert = UIAlertController(title: "", message: "本が見つかりませんでした", preferredStyle: .alert)
                self.present(alert, animated: true, completion: {// アラートを閉じる
                    self.activityView.stopAnimating()
                    self.activityView.isHidden = true//検索中停止
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        alert.dismiss(animated: true, completion: nil)
                    })
                })
                self.alertRock=0
                return
            }
            
            if(self.alertRock==1){
                let alert = UIAlertController(title: "", message: "本が"+String(self.bookCount)+"冊見つかりました", preferredStyle: .alert)
                self.present(alert, animated: true, completion: {// アラートを閉じる
                    self.activityView.stopAnimating()
                    self.activityView.isHidden=true//検索中停止
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        alert.dismiss(animated: true, completion: nil)
                    })
                })
                self.alertRock=0
            }
            self.parseData(resultSet:resultSet)
            
            //footerViewのindicatorを回したり、無くしたりしている。Dispatch必須。つけないと変なエラー出る。
            DispatchQueue.main.async {
                if(self.bookPageCount >= self.bookPage+1){
                    self.indicator.startAnimating()//ここで回さないと検索した時に既に回ってる状態になる。
                }else{
                    self.tableView.tableFooterView = UIView()
                }
            }
        }
        task.resume()//通信開始
    }
    
    func parseData(resultSet: [[String: Any]]){
        for item in resultSet{
            guard let volumeInfo=item["Item"] as? [String: Any] else{
                continue
            }
            let title=volumeInfo["title"] as? String //タイトル
            let authors=volumeInfo["author"] as? String //著書
            let publisher=volumeInfo["publisherName"] as? String //出版社
            let salesDate=volumeInfo["salesDate"] as? String //発売日
            let itemPrice=volumeInfo["itemPrice"] as? Int //値段
            let itemcaption=volumeInfo["itemCaption"] as? String //あらすじ
            let largeImageUrl=volumeInfo["largeImageUrl"] as? String //イメージリンク
            let affiliateUrl=volumeInfo["affiliateUrl"] as? String //アフィリエイトID
            let size = volumeInfo["size"] as? String
            let isbn = volumeInfo["isbn"] as? String
            let label = volumeInfo["seriesName"] as? String
            
            let book = BookData()
            book.dataTitle = title
            book.dataAuthor = authors
            book.dataPublisher = publisher
            book.dataSalesDate = salesDate
            book.dataItemPrice = String(itemPrice!)+"円"
            book.dataItemCaption = itemcaption
            book.dataImageLink = largeImageUrl
            book.dataAffiliateUrl = affiliateUrl
            book.dataSize = size
            book.dataISBN = isbn
            book.dataLabel = label
            
            //写真取得の処理 悩み中の処理
            guard var bookImageUrl = book.dataImageLink else{
                return
            }
            bookImageUrl = bookImageUrl.replacingOccurrences(of: "?_ex=200x200", with: "?_ex=400x400")
            guard let url=URL(string: bookImageUrl) else{
                return
            }
            let request=URLRequest(url: url)
            let session=URLSession.shared
            let task = session.dataTask(with: request){ (data:Data?,
                response: URLResponse?,error: Error?) in
                guard error == nil else{
                    return
                }
                guard let data = data else{
                    return
                }
                guard let image = UIImage(data: data) else{
                    return
                }
                DispatchQueue.main.async { //通信を用いているためDispatchQueueを用いる。いつ終わるかわからない。また処理が終わるまで固まるため。
                    book.dataImage = image
                    self.tableView.reloadData()//30回reloadしてる　危ない　出来たら直そう
                }
            }
            task.resume()
            /////////////////////////
            
            //配列に入力した値を挿入。
            searchBookList.insert(book, at: searchBookList.count)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.rock=0
        }
    }
    
    func errorAlert(title:String,message:String){
        DispatchQueue.main.async {
            self.activityView.stopAnimating()
            self.activityView.isHidden=true//検索中停止
            //アラートダイアログ生成
            let alertController=UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction=UIAlertAction(title:"OK",style: UIAlertAction.Style.cancel,handler: {
                (action: UIAlertAction!) -> Void in
            })
            alertController.addAction(okAction)
            self.present(alertController,animated: true,completion: nil)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tableView.contentOffset.y + tableView.frame.size.height > tableView.contentSize.height && tableView.isDragging&&rock==0{
            rock=1
            
            if(bookPageCount>=bookPage+1){
                let requestUrl = reloadUrl+"&page="+String(bookPage+1)+aplicationID
                //APIをリクエストする
                request(requestUrl:requestUrl)
                adLoader.load(GADRequest())//native広告
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //section使う時に必須のメソッドっぽい
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "あ"
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let search = UISearchBar()
        search.placeholder = "本のタイトルを入力してください"
        search.text = searchBar.text //渡ってくるテキストをさらに渡す。
        searchBar = search
        searchBar.delegate = self //これを書かないとsearchButtonClickedとかのdelegate動かない
        //searchControllerの周りのグレーを薄いグレーに変える処理。
        searchBar.barTintColor=UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1)
        //searchControllerの枠を黒から薄いグレーに変える処理。
        searchBar.layer.borderColor=UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1).cgColor
        searchBar.layer.borderWidth=1.0
        return searchBar
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    
    //セルの個数を指定するデリゲートメソッド(必須)
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        let adCount = searchBookList.count / 30
        return searchBookList.count + adCount
    }
    
    //セルに値を設定するデータソースメソッド(必須)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        //もしも31で割り切れた場合は広告を入れる。
        if (indexPath.row + 1) % 31 == 0 {
            let selectAd = (indexPath.row + 1) / 31 - 1
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdNativeCell",for: indexPath) as! AdNativeCell
            cell.selectionStyle = .none
            cell.nativeAdView.nativeAd = nativeAdData[selectAd]
            
            // Populate the native ad view with the native ad assets.
            // The headline is guaranteed to be present in every native ad.
            (cell.nativeAdView.headlineView as? UILabel)?.text = nativeAdData[selectAd].headline
            
            // These assets are not guaranteed to be present. Check that they are before
            // showing or hiding them.
            (cell.nativeAdView.bodyView as? UILabel)?.text = nativeAdData[selectAd].body
            cell.nativeAdView.bodyView?.isHidden = nativeAdData[selectAd].body == nil
            
            (cell.nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAdData[selectAd].callToAction, for: .normal)
            cell.nativeAdView.callToActionView?.isHidden = nativeAdData[selectAd].callToAction == nil
            
            if nativeAdData[selectAd].icon != nil {
                (cell.nativeAdView.iconView as? UIImageView)?.image = nativeAdData[selectAd].icon?.image
                //cell.nativeAdView.iconView?.isHidden = nativeAdData[selectAd].icon == nil
            }else{
                (cell.nativeAdView.iconView as? UIImageView)?.image = nativeAdData[selectAd].images?[0].image
            }
            
            (cell.nativeAdView.advertiserView as? UILabel)?.text = nativeAdData[selectAd].advertiser
            cell.nativeAdView.advertiserView?.isHidden = nativeAdData[selectAd].advertiser == nil
            
            // In order for the SDK to process touch events properly, user interaction
            // should be disabled.
            cell.nativeAdView.callToActionView?.isUserInteractionEnabled = false
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell",for: indexPath) as! SearchTableViewCell
            cell.delegate = self
            
            var count = (indexPath.row + 1) % 31
            count = (indexPath.row + 1) - count
            count = count / 31
            let book = searchBookList[indexPath.row - count]
            
            cell.bookTitle.text = book.dataTitle!
            cell.bookAuthor.text = book.dataAuthor!
            cell.bookImage.image = book.dataImage!
            cell.bookSalesDate.text = book.dataSalesDate!
            cell.bookPrice.text = book.dataItemPrice!
            
            //セルのハイライト色をグレーから薄い青に変更する処理。
            let selectedBackgroundView: UIView = UIView()
            selectedBackgroundView.backgroundColor = UIColor(red: 0, green: 135/255, blue: 1, alpha: 0.3)
            cell.selectedBackgroundView = selectedBackgroundView
            
            return cell
        }
    }
    
    /*ver4.0.1 SwipeAction*/
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let addBookAction = SwipeAction(style: .destructive, title: "登録") { action, indexPath in
            action.fulfill(with: .reset)
            self.sameBookAlert(indexPath: indexPath)//同じ本があるかどうかを調べてから本を追加
        }
        addBookAction.image = UIImage(named: "registImage")
        addBookAction.backgroundColor = UIColor(red: 1, green: 130/255, blue: 0, alpha: 1)
        
        return [addBookAction]
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive
        options.transitionStyle = .border
        return options
    }
    /********************/
    
    
    func sameBookAlert(indexPath: IndexPath){
        var title = ""
        let realm = try! Realm()
        
        var count = (indexPath.row + 1) % 31
        count = (indexPath.row + 1) - count
        count = count / 31

        let bookResult:Int = realm.objects(RealmBookData.self).filter("dataISBN = '\(searchBookList[indexPath.row - count].dataISBN!)'").count
        if(bookResult >= 1){
            title = "本棚に登録されています"
        }
        
        if(title != ""){
            let alertController=UIAlertController(title: title, message: "新たに登録しますか？", preferredStyle: UIAlertController.Style.alert)
            let okAction=UIAlertAction(title:"OK",style: UIAlertAction.Style.default,handler: {
                (action: UIAlertAction!) -> Void in
                //                self.editActionBookSave(indexPath: indexPath)
                self.bookAlertSheet(indexPath: indexPath)
            })
            let cancel=UIAlertAction(title:"キャンセル",style: UIAlertAction.Style.cancel,handler: {
                (action: UIAlertAction!) -> Void in
                self.tableView.reloadData()
            })
            alertController.addAction(okAction)
            alertController.addAction(cancel)
            self.present(alertController,animated: true,completion: nil)
        }else{
            //            self.editActionBookSave(indexPath: indexPath)
            self.bookAlertSheet(indexPath: indexPath)
            
        }
    }
    
    func editActionBookSave(indexPath: IndexPath,dataStatus: String,bookTagTextField: String){
        let now = NSDate() // 現在日時の取得
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "ja_JP") as Locale? // ロケールの設定
        dateFormatter.dateFormat = "yyyy/MM/dd" // 日付フォーマットの設定

        var count = (indexPath.row + 1) % 31
        count = (indexPath.row + 1) - count
        count = count / 31
        
        self.searchBookList[indexPath.row - count].dataRegistDate=dateFormatter.string(from: (now as Date) as Date)//登録日
        //本棚に追加
        let realm = try! Realm()
        try! realm.write {
            let bookData = RealmBookData()
            bookData.dataTitle = self.searchBookList[indexPath.row - count].dataTitle!
            bookData.dataAuthor = self.searchBookList[indexPath.row - count].dataAuthor!
            bookData.dataImageLink = self.searchBookList[indexPath.row - count].dataImageLink!
            bookData.dataImage = self.searchBookList[indexPath.row - count].dataImage!.pngData()! as NSData
            bookData.dataPublisher = self.searchBookList[indexPath.row - count].dataPublisher!
            bookData.dataSalesDate = self.searchBookList[indexPath.row - count].dataSalesDate!
            bookData.dataRegistDate = self.searchBookList[indexPath.row - count].dataRegistDate!
            bookData.dataItemPrice = self.searchBookList[indexPath.row - count].dataItemPrice!
            bookData.dataItemCaption = self.searchBookList[indexPath.row - count].dataItemCaption!
            bookData.dataImpression = self.searchBookList[indexPath.row - count].dataImpression!
            bookData.dataAffiliateUrl = self.searchBookList[indexPath.row - count].dataAffiliateUrl!
            bookData.dataReadStartDay = ""
            bookData.dataReadEndDay = ""
            bookData.dataSize = self.searchBookList[indexPath.row - count].dataSize!
            bookData.dataISBN = self.searchBookList[indexPath.row - count].dataISBN!
            bookData.dataLabel = self.searchBookList[indexPath.row - count].dataLabel!
            bookData.dataStatus = dataStatus
            bookData.dataRegistNumber = bookRegistNumber
            bookData.dataCollection = bookTagTextField
            realm.add(bookData)
        }
        bookTableView.reloadData()
        
        let alert = UIAlertController(title: "", message: "本棚に登録しました", preferredStyle: .alert)
        self.present(alert, animated: true, completion: {// アラートを閉じる
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        })
        self.tableView.reloadData()
        
        self.saveBookRegistNumber()
    }
    
    func bookAlertSheet(indexPath:IndexPath){
        let alertController=UIAlertController(title: "本棚に登録", message: "タグと読書状況を入力してください", preferredStyle: UIAlertController.Style.alert)
        var titleName:[String] = ["欲しい","読んでいる","未読","既読","登録をやめる"]
        alertController.addTextField{(textFieldA) -> Void in
            self.textField = textFieldA
            self.textField.delegate = self
            self.textField.placeholder = "タグを入力してください"
            self.textField.textAlignment = .center
            self.textField.isEnabled = false
            
            let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
            let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
            let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let history = UIBarButtonItem(title: "履歴", style: .plain, target: self, action: #selector(self.history))
            toolbar.setItems([doneItem,flexibleItem,history], animated: true)
            self.textField.inputAccessoryView = toolbar
            
            if let tagHistory:[String] = UserDefaults.standard.stringArray(forKey: "TagHistoryArray"){
                self.tagHistoryArray = tagHistory
            }
        }
        let defaultAction = UIAlertAction(title: titleName[0],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(indexPath: indexPath, dataStatus: titleName[0], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let defaultAction1 = UIAlertAction(title: titleName[1],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(indexPath: indexPath, dataStatus: titleName[1], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let defaultAction2 = UIAlertAction(title: titleName[2],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(indexPath: indexPath, dataStatus: titleName[2], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let defaultAction3 = UIAlertAction(title: titleName[3],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(indexPath: indexPath, dataStatus: titleName[3], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let cancel=UIAlertAction(title: titleName[4],style: UIAlertAction.Style.destructive,handler: {
            (action: UIAlertAction!) -> Void in
            self.tableView.reloadData()
        })
        alertController.addAction(defaultAction)
        alertController.addAction(defaultAction1)
        alertController.addAction(defaultAction2)
        alertController.addAction(defaultAction3)
        alertController.addAction(cancel)
        self.present(alertController,animated: true,completion: {
            alertController.textFields?.first?.isEnabled = true
        })
    }
    
    //タグの履歴を保存する処理。
    func tagHistoryArraySave(text: String){
        if(tagHistoryArray.firstIndex(of: text) == nil && text != ""){//履歴にないデータ　かつ　未入力は入れない
            tagHistoryArray.insert(text, at: 0)
            UserDefaults.standard.set(tagHistoryArray, forKey: "TagHistoryArray")
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let searchBookDetailView:BookDetailViewController = segue.destination as! BookDetailViewController
        if let indexPath = self.tableView.indexPathForSelectedRow{

            var count = (indexPath.row + 1) % 31
            count = (indexPath.row + 1) - count
            count = count / 31
            
            searchBookDetailView.image=searchBookList[indexPath.row - count].dataImage!
            searchBookDetailView.detailTitle=searchBookList[indexPath.row - count].dataTitle!
            searchBookDetailView.author=searchBookList[indexPath.row - count].dataAuthor!
            searchBookDetailView.publisher=searchBookList[indexPath.row - count].dataPublisher!
            searchBookDetailView.label = searchBookList[indexPath.row - count].dataLabel!
            searchBookDetailView.size = searchBookList[indexPath.row - count].dataSize!
            searchBookDetailView.salesDate=searchBookList[indexPath.row - count].dataSalesDate!
            searchBookDetailView.itemPrice=searchBookList[indexPath.row - count].dataItemPrice!
            searchBookDetailView.isbn = searchBookList[indexPath.row - count].dataISBN!
            searchBookDetailView.itemCaption=searchBookList[indexPath.row - count].dataItemCaption!
            searchBookDetailView.affiliateUrl=searchBookList[indexPath.row - count].dataAffiliateUrl!
            
            searchBookDetailView.indexPath = indexPath
        }
    }
}

//タグの履歴表示のためのクラス
extension SearchViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //表示する数
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return tagHistoryArray.count + 1
    }
    //表示するためのもの
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (row == 0) {
            return ""
        }else{
            return tagHistoryArray[row - 1]
        }
    }
    //選択された時
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row != 0 {
            textField.text = tagHistoryArray[row - 1]
        }
    }
    
    @objc func done() {
        textField.endEditing(true)
    }
    
    @objc func history(){
        //PickerViewの設定
        let pickerView: UIPickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.showsSelectionIndicator = true
        self.textField.inputView = pickerView
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let history = UIBarButtonItem(title: "キー", style: .plain, target: self, action: #selector(self.keyboard))
        toolbar.setItems([doneItem,flexibleItem,history], animated: true)
        self.textField.inputAccessoryView = toolbar
        
        self.textField.reloadInputViews()
    }
    
    @objc func keyboard() {
        //https://starzero.hatenablog.com/entry/2014/08/05/170122 普通のキーボードに戻す処理
        self.textField.inputView = nil
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let history = UIBarButtonItem(title: "履歴", style: .plain, target: self, action: #selector(self.history))
        toolbar.setItems([doneItem,flexibleItem,history], animated: true)
        self.textField.inputAccessoryView = toolbar
        
        self.textField.reloadInputViews()
    }
}

