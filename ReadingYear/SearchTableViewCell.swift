//
//  SearchTableViewCell.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/05/12.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import SwipeCellKit

class SearchTableViewCell: SwipeTableViewCell {

    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookTitle: UILabel!
    @IBOutlet weak var bookAuthor: UILabel!
    @IBOutlet weak var bookSalesDate: UILabel!
    @IBOutlet weak var bookPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
