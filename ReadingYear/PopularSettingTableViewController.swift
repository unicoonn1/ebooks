//
//  PopularSettingTableViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/11/19.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit

class PopularSettingTableViewController: UITableViewController{
    
    var frontVC:PopularTableViewController = PopularTableViewController()
    var bookGenre:String = "001001"
    var changeGenreFlag:String = "001001"
    
    let titleArray:[String] = ["漫画(コミック)","ライトノベル","小説・エッセイ","文庫","新書","人文・思想・社会","ビジネス・経済・就職","美容・暮らし・健康・料理","旅行・留学・アウトドア","ホビー・スポーツ・美術","医学・薬学・看護学・歯科学","科学・技術","パソコン・システム開発","語学・学習参考書","資格・検定","絵本・児童書・図鑑","写真集・タレント","エンタメ・ゲーム","楽譜","付録付き","バーゲン本","コミックセット","カレンダー・手帳・家計簿","文具・雑貨","ボーイズラブ","その他"]
    
    let numberArray:[String] = ["001001","001017","001004","001019","001020","001008","001006","001010","001007","001009","001028","001012","001005","001002","001016","001003","001013","001011","001018","001022","001023","001025","001026","001027","001021","001015"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: .zero)
        let color:UIColor=UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        tableView.backgroundColor=color
        
        if let genre:String = UserDefaults.standard.object(forKey: "bookGenre") as? String{
            bookGenre = genre
            changeGenreFlag = genre
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func stopButton(_ sender: UIBarButtonItem) {
        if(bookGenre != changeGenreFlag){
            frontVC.searchBook()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.setValue(numberArray[indexPath.row], forKey: "bookGenre")
        bookGenre = numberArray[indexPath.row]
        tableView.reloadData()
        
        frontVC.searchBook()
        self.dismiss(animated: true, completion: nil)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "basic", for: indexPath)
        cell.textLabel?.text = titleArray[indexPath.row]
        if(bookGenre == numberArray[indexPath.row]){
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        
        //セルのハイライト色をグレーから薄い青に変更する処理。
        let selectedBackgroundView: UIView = UIView()
        selectedBackgroundView.backgroundColor = UIColor(red: 0, green: 135/255, blue: 1, alpha: 0.3)
        cell.selectedBackgroundView = selectedBackgroundView
        return cell
    }
}
