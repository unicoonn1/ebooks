//
//  DetailSearchTableViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/10/19.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit

class DetailSearchTableViewController: UITableViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{

    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var authorText: UITextField!
    @IBOutlet weak var publisherText: UITextField!
    @IBOutlet weak var isbnText: UITextField!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var sortLabel: UILabel!
    
    var sortNumber:Int = 0
    var bookSize:Int = 0
    
    var sizeText:UITextView = UITextView()
    var pickerViewSelect:Int = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //遷移先から戻るの時の名前をbackから戻るに変更
        let backButtonItem = UIBarButtonItem(title: "戻る", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem
        
        tableView.tableFooterView = UIView(frame: .zero)
        let color:UIColor=UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        tableView.backgroundColor=color
        
        for i in 0..<12 {
            let indexPath = IndexPath(row: i, section: 0)
            let cell = self.tableView(self.tableView, cellForRowAt: indexPath)
            //セルのハイライト色をグレーから薄い青に変更する処理。
            let selectedBackgroundView: UIView = UIView()
            selectedBackgroundView.backgroundColor = UIColor(red: 0, green: 135/255, blue: 1, alpha: 0.3)
            cell.selectedBackgroundView = selectedBackgroundView
        }
        //UITextFieldでkeyboardを表示した時に、完了でkeyboardを下げる処理。
        titleText.delegate = self
        authorText.delegate = self
        publisherText.delegate = self
        isbnText.delegate = self
        
        
        let sizeTextView = UIPickerView()
        sizeTextView.delegate = self
        sizeText.inputView = sizeTextView

        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0, green: 120/255, blue: 1, alpha: 1)
        let doneButton   = UIBarButtonItem(title: "完了", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePressed))
        let cancelButton = UIBarButtonItem(title: "キャンセル", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelPressed))
        let spaceButton  = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        sizeText.inputAccessoryView = toolBar
        self.view.addSubview(sizeText)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    let sizeArray:[String] = ["全て","単行本","文庫","新書","全集・双書","事・辞典","図鑑","絵本","カセット・CDなど","コミック","その他"]
    //PickerViewのコンポーネントの数を決めるメソッド
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //PickerViewのコンポーネント内の行数を決めるメソッド
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sizeArray.count
    }
    //PickerViewのコンポーネントに表示するデータを決めるメソッド
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sizeArray[row]
    }
    //PickerView選択時の処理
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerViewSelect = row
    }
    
    // Done
    @objc func donePressed() {
        bookSize = pickerViewSelect
        let indexPath = IndexPath(row: 9, section: 0)
        tableView.deselectRow(at: indexPath, animated: true)
        self.sizeLabel.text = sizeArray[pickerViewSelect]
        
        view.endEditing(true)
    }
    
    // Cancel
    @objc func cancelPressed() {
        let indexPath = IndexPath(row: 9, section: 0)
        tableView.deselectRow(at: indexPath, animated: true)
        
        view.endEditing(true)
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 9){
            sizeText.becomeFirstResponder()
        }else if(indexPath.row == 11){
            let alert: UIAlertController = UIAlertController(title: "並び替え", message: .none, preferredStyle:  UIAlertController.Style.actionSheet)
            
            let defaultAction: UIAlertAction = UIAlertAction(title: "標準順", style: UIAlertAction.Style.default, handler:{
                (action: UIAlertAction!) -> Void in
                self.sortNumber = 0
                self.sortLabel.text = "標準順"
                tableView.deselectRow(at: indexPath, animated: true)
            })
            let defaultAction1: UIAlertAction = UIAlertAction(title: "人気順", style: UIAlertAction.Style.default, handler:{
                (action: UIAlertAction!) -> Void in
                self.sortNumber = 1
                self.sortLabel.text = "人気順"
                tableView.deselectRow(at: indexPath, animated: true)
            })
            let defaultAction2: UIAlertAction = UIAlertAction(title: "発売日が新しい順", style: UIAlertAction.Style.default, handler:{
                (action: UIAlertAction!) -> Void in
                self.sortNumber = 2
                self.sortLabel.text = "発売日が新しい順"
                tableView.deselectRow(at: indexPath, animated: true)
            })
            let defaultAction3: UIAlertAction = UIAlertAction(title: "発売日が古い順", style: UIAlertAction.Style.default, handler:{
                (action: UIAlertAction!) -> Void in
                self.sortNumber = 3
                self.sortLabel.text = "発売日が古い順"
                tableView.deselectRow(at: indexPath, animated: true)
            })
            let defaultAction4: UIAlertAction = UIAlertAction(title: "価格が高い順", style: UIAlertAction.Style.default, handler:{
                (action: UIAlertAction!) -> Void in
                self.sortNumber = 4
                self.sortLabel.text = "価格が高い順"
                tableView.deselectRow(at: indexPath, animated: true)
            })
            let defaultAction5: UIAlertAction = UIAlertAction(title: "価格が低い順", style: UIAlertAction.Style.default, handler:{
                (action: UIAlertAction!) -> Void in
                self.sortNumber = 5
                self.sortLabel.text = "価格が低い順"
                tableView.deselectRow(at: indexPath, animated: true)
            })
            let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler:{
                (action: UIAlertAction!) -> Void in

                tableView.deselectRow(at: indexPath, animated: true)
            })
            
            if(self.sortNumber == 0){
                defaultAction.setValue(UIImage(named: "todo.png"), forKey: "image")
                defaultAction.setValue(UIColor.orange, forKey: "imageTintColor")
                defaultAction.setValue(UIColor.orange, forKey: "titleTextColor")
            }else if(self.sortNumber == 1){
                defaultAction1.setValue(UIImage(named: "todo.png"), forKey: "image")
                defaultAction1.setValue(UIColor.orange, forKey: "imageTintColor")
                defaultAction1.setValue(UIColor.orange, forKey: "titleTextColor")
            }else if(self.sortNumber == 2){
                defaultAction2.setValue(UIImage(named: "todo.png"), forKey: "image")
                defaultAction2.setValue(UIColor.orange, forKey: "imageTintColor")
                defaultAction2.setValue(UIColor.orange, forKey: "titleTextColor")
            }else if(self.sortNumber == 3){
                defaultAction3.setValue(UIImage(named: "todo.png"), forKey: "image")
                defaultAction3.setValue(UIColor.orange, forKey: "imageTintColor")
                defaultAction3.setValue(UIColor.orange, forKey: "titleTextColor")
            }else if(self.sortNumber == 4){
                defaultAction4.setValue(UIImage(named: "todo.png"), forKey: "image")
                defaultAction4.setValue(UIColor.orange, forKey: "imageTintColor")
                defaultAction4.setValue(UIColor.orange, forKey: "titleTextColor")
            }else{
                defaultAction5.setValue(UIImage(named: "todo.png"), forKey: "image")
                defaultAction5.setValue(UIColor.orange, forKey: "imageTintColor")
                defaultAction5.setValue(UIColor.orange, forKey: "titleTextColor")
            }
            alert.addAction(defaultAction)
            alert.addAction(defaultAction1)
            alert.addAction(defaultAction2)
            alert.addAction(defaultAction3)
            alert.addAction(defaultAction4)
            alert.addAction(defaultAction5)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailSearchBookTVC:DetailSearchBookTableViewController = segue.destination as! DetailSearchBookTableViewController
        
        detailSearchBookTVC.searchTitle = titleText.text!
        detailSearchBookTVC.searchAuthor = authorText.text!
        detailSearchBookTVC.searchPublisher = publisherText.text!
        detailSearchBookTVC.searchISBN = isbnText.text!
        detailSearchBookTVC.bookSize = bookSize
        detailSearchBookTVC.sortNumber = sortNumber
        
        self.view.endEditing(true)
    }
}
