//
//  PopularTableViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/10/25.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import RealmSwift
import SwipeCellKit
import GoogleMobileAds

var popularTableView:UITableView!

class PopularTableViewController: UITableViewController,UITextFieldDelegate,SwipeTableViewCellDelegate,GADUnifiedNativeAdLoaderDelegate{

    var bookGenre:String = "001001"
    let titleArray:[String] = ["漫画(コミック)","ライトノベル","小説・エッセイ","文庫","新書","人文・思想・社会","ビジネス・経済・就職","美容・暮らし・健康・料理","旅行・留学・アウトドア","ホビー・スポーツ・美術","医学・薬学・看護学・歯科学","科学・技術","パソコン・システム開発","語学・学習参考書","資格・検定","絵本・児童書・図鑑","写真集・タレント","エンタメ・ゲーム","楽譜","付録付き","バーゲン本","コミックセット","カレンダー・手帳・家計簿","文具・雑貨","ボーイズラブ","その他"]
    
    let numberArray:[String] = ["001001","001017","001004","001019","001020","001008","001006","001010","001007","001009","001028","001012","001005","001002","001016","001003","001013","001011","001018","001022","001023","001025","001026","001027","001021","001015"]
    
    //APIのURL
    var entryUrl: String = "https://app.rakuten.co.jp/services/api/BooksBook/Search/20170404?format=json&sort=sales"
    var aplicationID:String = "&outOfStockFlag=1&affiliateId=15be4ccf.cc97e6e6.15be4cd0.efe26c07&applicationId=1014949399335024209"
    var searchBookList = [BookData]()
    var reloadUrl = ""
    
    var bookPage:Int = 0
    var bookPageCount:Int = 0
    var bookCount:Int = 0
    var rock:Int = 0
    var alertRock:Int = 0
    var searchBarText = ""
    
    var activityView:UIActivityIndicatorView!
    var indicator: UIActivityIndicatorView!
    var footerView: UIView!
    
    //tagのためのtextField
    var textField: UITextField!
    //このリストにタグの履歴のデータを入れる。
    var tagHistoryArray:[String] = []
    
    //広告用
    var adLoader: GADAdLoader!
    var nativeAdData: [GADUnifiedNativeAd] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let option = GADNativeAdMediaAdLoaderOptions()
        option.mediaAspectRatio = .portrait
        //本番
        let adID: String = "ca-app-pub-9383479591463806/3309007002"
        //テスト
//        let adID: String = "ca-app-pub-3940256099942544/3986624511"
        adLoader = GADAdLoader(adUnitID: adID,
                               rootViewController: self,
                               adTypes: [GADAdLoaderAdType.unifiedNative],
                               options: [option])
        adLoader.delegate = self
//        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ "2077ef9a63d2b398840261c8221a0c9b" ]
        adLoader.load(GADRequest())
        
        //カスタムセルを使う際に必須の処理
        self.tableView.register(UINib(nibName: "AdNativeCell", bundle: nil), forCellReuseIdentifier: "AdNativeCell")
                
        //遷移先から戻るの時の名前をbackから戻るに変更
        let backButtonItem = UIBarButtonItem(title: "戻る", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem
        
        tableView.tableFooterView = UIView(frame: .zero)
        let color:UIColor=UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        tableView.backgroundColor=color
        
        activityView = UIActivityIndicatorView(style: .whiteLarge)
        activityView.color = UIColor.darkGray
        //訳わからん 64pt下げないと位置が合わない
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        let navBarHeight = self.navigationController?.navigationBar.frame.height
        let height = statusBarHeight + navBarHeight!
        activityView.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2 - height)
        self.view.addSubview(activityView)
        
        popularTableView = self.tableView
        
        self.searchBook()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //広告の処理
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
        nativeAdData.append(nativeAd)
        tableView.reloadData()
    }
    
    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        // The adLoader has finished loading ads, and a new request can be sent.
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
        print(error);
    }

    
    //戻った時にセルの選択状態を解除する処理。
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //セルの選択状態を解除する処理。　戻った時にどのセルを選択していたかを見やすくするための処理。
        if let indexPathForSelectedRow = self.tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPathForSelectedRow, animated: true)
        }
    }    
    
    @IBAction func changeViewButton(_ sender: UIBarButtonItem) {
        let storyboard: UIStoryboard = self.storyboard!
        let navigationView = storyboard.instantiateViewController(withIdentifier: "navi") as! UINavigationController
        let settingTVC = navigationView.children[0] as? PopularSettingTableViewController
        settingTVC?.frontVC = self
        self.present(navigationView, animated: true, completion: nil)
    }
    
    func saveBookRegistNumber(){
        bookRegistNumber += 1
        let userDefaults = UserDefaults.standard
        userDefaults.set(bookRegistNumber, forKey: "bookRegistNumber")
    }
    
    @IBAction func refreshButton(_ sender: UIBarButtonItem) {
        self.searchBook()
    }
    
    func searchBook() {
        activityView.startAnimating()
        activityView.isHidden = false//クルクル回し開始、検索中
        searchBookList.removeAll()
        self.tableView.reloadData()
        
        if let genre:String = UserDefaults.standard.object(forKey: "bookGenre") as? String{
            bookGenre = genre
        }
        
        //リファクタリングして見やすくしたほうがいいかも
        //初回のリクエスト
        var requestUrl = entryUrl + "&booksGenreId=" + String(bookGenre)
        
        //2回目以降のリクエスト用の処理。
        reloadUrl = requestUrl
        //初回のリクエストURL完成
        requestUrl = requestUrl + aplicationID
        
        alertRock=1
        //APIをリクエストする
        request(requestUrl:requestUrl)
        
        
        //footerviewにindicatorをaddsubviewし、ぐるぐる表示をしている。ここに書いているのはあまり良くない気がする。
        footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 60))
        indicator = UIActivityIndicatorView(style: .white)
        indicator.color = UIColor.darkGray
        indicator.center = CGPoint(x: self.view.bounds.width/2, y: 30)
        footerView.addSubview(self.indicator)
        tableView.tableFooterView = footerView
    }
    
    func request(requestUrl: String){
        //URL生成
        guard let url=URL(string: requestUrl) else{
            return
        }
        //リクエスト生成
        let request=URLRequest(url: url)
        let session=URLSession.shared
        let task=session.dataTask(with: request){(data:Data?,response:URLResponse?,error:Error?) in
            //通信完了後の処理
            //エラーチェック
            guard error==nil else{
                //エラー処理かく
                self.errorAlert(title: "エラー", message: "データがありません。")
                return
            }
            
            //JSONで返却されたデータをパースして格納する
            guard let data=data else{
                //データなし
                self.errorAlert(title: "データ無し", message: "データがありません。")
                return
            }
            
            //JSON形式への変換処理
            guard let jsonData = try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any] else{
                //変換失敗
                self.errorAlert(title: "変換失敗", message: "変換処理が出来ません。")
                return
            }
            
            //データ解析
            guard let resultSet=jsonData["Items"] as? [[String:Any]] else{
                //データなし
                self.errorAlert(title: "データ無し", message: "データがありません。")
                return
            }
            
            guard let page=jsonData["page"] as? Int else{
                return
            }
            self.bookPage=page
            guard let pageCount=jsonData["pageCount"] as? Int else{
                return
            }
            self.bookPageCount=pageCount
            guard let count=jsonData["count"] as? Int else{
                return
            }
            self.bookCount=count
            
            if(self.alertRock==1&&self.bookCount==0){//0冊だった時の処理。
                let alert = UIAlertController(title: "", message: "本が見つかりませんでした", preferredStyle: .alert)
                self.present(alert, animated: true, completion: {// アラートを閉じる
                    self.activityView.stopAnimating()
                    self.activityView.isHidden = true//検索中停止
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        alert.dismiss(animated: true, completion: nil)
                    })
                })
                self.alertRock=0
                return
            }
            
            if(self.alertRock==1){
                let alert = UIAlertController(title: "", message: "本が"+String(self.bookCount)+"冊見つかりました", preferredStyle: .alert)
                self.present(alert, animated: true, completion: {// アラートを閉じる
                    self.activityView.stopAnimating()//これつけないとタブ間移動した後に戻るとなぜかactivityview表示されている
                    self.activityView.isHidden = true//検索中停止
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        alert.dismiss(animated: true, completion: nil)
                    })
                })
                self.alertRock = 0
            }
            self.parseData(resultSet:resultSet)
            
            //footerViewのindicatorを回したり、無くしたりしている。Dispatch必須。つけないと変なエラー出る。
            DispatchQueue.main.async {
                if(self.bookPageCount >= self.bookPage+1){
                    self.indicator.startAnimating()//ここで回さないと検索した時に既に回ってる状態になる。
                }else{
                    self.tableView.tableFooterView = UIView()
                }
            }
        }
        task.resume()//通信開始
    }
    
    func parseData(resultSet: [[String: Any]]){
        for item in resultSet{
            guard let volumeInfo=item["Item"] as? [String: Any] else{
                continue
            }
            let title=volumeInfo["title"] as? String //タイトル
            let authors=volumeInfo["author"] as? String //著書
            let publisher=volumeInfo["publisherName"] as? String //出版社
            let salesDate=volumeInfo["salesDate"] as? String //発売日
            let itemPrice=volumeInfo["itemPrice"] as? Int //値段
            let itemcaption=volumeInfo["itemCaption"] as? String //あらすじ
            let largeImageUrl=volumeInfo["largeImageUrl"] as? String //イメージリンク
            let affiliateUrl=volumeInfo["affiliateUrl"] as? String //アフィリエイトID
            let size = volumeInfo["size"] as? String
            let isbn = volumeInfo["isbn"] as? String
            let label = volumeInfo["seriesName"] as? String
            
            let book = BookData()
            book.dataTitle = title
            book.dataAuthor = authors
            book.dataPublisher = publisher
            book.dataSalesDate = salesDate
            book.dataItemPrice = String(itemPrice!)+"円"
            book.dataItemCaption = itemcaption
            book.dataImageLink = largeImageUrl
            book.dataAffiliateUrl = affiliateUrl
            book.dataSize = size
            book.dataISBN = isbn
            book.dataLabel = label
            
            //写真取得の処理 悩み中の処理
            guard var bookImageUrl = book.dataImageLink else{
                return
            }
            bookImageUrl = bookImageUrl.replacingOccurrences(of: "?_ex=200x200", with: "?_ex=400x400")
            guard let url=URL(string: bookImageUrl) else{
                return
            }
            let request=URLRequest(url: url)
            let session=URLSession.shared
            let task = session.dataTask(with: request){ (data:Data?,
                response: URLResponse?,error: Error?) in
                guard error == nil else{
                    return
                }
                guard let data = data else{
                    return
                }
                guard let image = UIImage(data: data) else{
                    return
                }
                DispatchQueue.main.async { //通信を用いているためDispatchQueueを用いる。いつ終わるかわからない。また処理が終わるまで固まるため。
                    book.dataImage = image
                    self.tableView.reloadData()//30回reloadしてる　危ない　出来たら直そう
                }
            }
            task.resume()
            /////////////////////////
            
            //配列に入力した値を挿入。
            searchBookList.insert(book, at: searchBookList.count)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.rock=0
        }
    }
    
    func errorAlert(title:String,message:String){
        DispatchQueue.main.async {
            self.activityView.isHidden=true//検索中停止
            //アラートダイアログ生成
            let alertController=UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction=UIAlertAction(title:"OK",style: UIAlertAction.Style.cancel,handler: {
                (action: UIAlertAction!) -> Void in
            })
            alertController.addAction(okAction)
            self.present(alertController,animated: true,completion: nil)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tableView.contentOffset.y + tableView.frame.size.height > tableView.contentSize.height && tableView.isDragging&&rock==0{
            rock=1
            
            if(bookPageCount>=bookPage+1){
                let requestUrl = reloadUrl+"&page="+String(bookPage+1)+aplicationID
                //APIをリクエストする
                request(requestUrl:requestUrl)
                adLoader.load(GADRequest())//native広告
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return bookGenre
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label : UILabel = UILabel()
        label.backgroundColor = UIColor(red: 0, green: 160/255, blue: 1, alpha: 1)
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 14)//UIのフォントとサイズ
        for i in 0..<numberArray.count{
            if(bookGenre == numberArray[i]){
                label.text = "  " + titleArray[i]
            }
        }
        return label
    }

    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //セルの個数を指定するデリゲートメソッド(必須)
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        //30ごとに広告を一個挿入する。
        let adCount = searchBookList.count / 30
        return searchBookList.count + adCount
    }
    
    //セルに値を設定するデータソースメソッド(必須)
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        //もしも31で割り切れた場合は広告を入れる。
        if (indexPath.row + 1) % 31 == 0 {
            let selectAd = (indexPath.row + 1) / 31 - 1
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdNativeCell",for: indexPath) as! AdNativeCell
            cell.selectionStyle = .none
            cell.nativeAdView.nativeAd = nativeAdData[selectAd]

            // Populate the native ad view with the native ad assets.
            // The headline is guaranteed to be present in every native ad.
            (cell.nativeAdView.headlineView as? UILabel)?.text = nativeAdData[selectAd].headline
            
            // These assets are not guaranteed to be present. Check that they are before
            // showing or hiding them.
            (cell.nativeAdView.bodyView as? UILabel)?.text = nativeAdData[selectAd].body
            cell.nativeAdView.bodyView?.isHidden = nativeAdData[selectAd].body == nil
            
            (cell.nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAdData[selectAd].callToAction, for: .normal)
            cell.nativeAdView.callToActionView?.isHidden = nativeAdData[selectAd].callToAction == nil
            
            if nativeAdData[selectAd].icon != nil {
                (cell.nativeAdView.iconView as? UIImageView)?.image = nativeAdData[selectAd].icon?.image
                //cell.nativeAdView.iconView?.isHidden = nativeAdData[selectAd].icon == nil
            }else{
                (cell.nativeAdView.iconView as? UIImageView)?.image = nativeAdData[selectAd].images?[0].image
            }
            
            (cell.nativeAdView.advertiserView as? UILabel)?.text = nativeAdData[selectAd].advertiser
            cell.nativeAdView.advertiserView?.isHidden = nativeAdData[selectAd].advertiser == nil
            
            // In order for the SDK to process touch events properly, user interaction
            // should be disabled.
            cell.nativeAdView.callToActionView?.isUserInteractionEnabled = false
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! PopularTableViewCell
            cell.delegate = self//SwipecellKit追加
            
            var count = (indexPath.row + 1) % 31
            count = (indexPath.row + 1) - count
            count = count / 31
            let book = searchBookList[indexPath.row - count]
            
            cell.bookImage.image = book.dataImage!
            cell.bookTitle.text = book.dataTitle!
            cell.bookAuthor.text = book.dataAuthor!
            cell.bookSalesDate.text = book.dataSalesDate!
            cell.bookPrice.text = book.dataItemPrice!
            
            //セルのハイライト色をグレーから薄い青に変更する処理。
            let selectedBackgroundView: UIView = UIView()
            selectedBackgroundView.backgroundColor = UIColor(red: 0, green: 135/255, blue: 1, alpha: 0.3)
            cell.selectedBackgroundView = selectedBackgroundView
            
            return cell
        }
    }

    /*ver4.0.1 SwipeAction*/
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let addBookAction = SwipeAction(style: .destructive, title: "登録") { action, indexPath in
            action.fulfill(with: .reset)
            self.sameBookAlert(indexPath: indexPath)//同じ本があるかどうかを調べてから本を追加
        }
        addBookAction.image = UIImage(named: "registImage")
        addBookAction.backgroundColor = UIColor(red: 1, green: 130/255, blue: 0, alpha: 1)
        
        return [addBookAction]
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive
        options.transitionStyle = .border
        return options
    }
    /***************************************/
    
    func sameBookAlert(indexPath: IndexPath){
        var title = ""
        let realm = try! Realm()
        
        var count = (indexPath.row + 1) % 31
        count = (indexPath.row + 1) - count
        count = count / 31

        let bookResult:Int = realm.objects(RealmBookData.self).filter("dataISBN = '\(searchBookList[indexPath.row - count].dataISBN!)'").count
        if(bookResult >= 1){
            title = "本棚に登録されています"
        }
        
        if(title != ""){
            let alertController=UIAlertController(title: title, message: "新たに登録しますか？", preferredStyle: UIAlertController.Style.alert)
            let okAction=UIAlertAction(title:"OK",style: UIAlertAction.Style.default,handler: {
                (action: UIAlertAction!) -> Void in
                self.bookAlertSheet(indexPath: indexPath)
            })
            let cancel=UIAlertAction(title:"キャンセル",style: UIAlertAction.Style.cancel,handler: {
                (action: UIAlertAction!) -> Void in
                self.tableView.reloadData()
            })
            alertController.addAction(okAction)
            alertController.addAction(cancel)
            self.present(alertController,animated: true,completion: nil)
        }else{
            self.bookAlertSheet(indexPath: indexPath)
        }
    }
    
    func editActionBookSave(indexPath: IndexPath,dataStatus: String,bookTagTextField: String){
        let now = NSDate() // 現在日時の取得
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "ja_JP") as Locale? // ロケールの設定
        dateFormatter.dateFormat = "yyyy/MM/dd" // 日付フォーマットの設定

        var count = (indexPath.row + 1) % 31
        count = (indexPath.row + 1) - count
        count = count / 31
        
        self.searchBookList[indexPath.row - count].dataRegistDate=dateFormatter.string(from: (now as Date) as Date)//登録日
        //本棚に追加
        let realm = try! Realm()
        try! realm.write {
            let bookData = RealmBookData()
            bookData.dataTitle = self.searchBookList[indexPath.row - count].dataTitle!
            bookData.dataAuthor = self.searchBookList[indexPath.row - count].dataAuthor!
            bookData.dataImageLink = self.searchBookList[indexPath.row - count].dataImageLink!
            bookData.dataImage = self.searchBookList[indexPath.row - count].dataImage!.pngData()! as NSData
            bookData.dataPublisher = self.searchBookList[indexPath.row - count].dataPublisher!
            bookData.dataSalesDate = self.searchBookList[indexPath.row - count].dataSalesDate!
            bookData.dataRegistDate = self.searchBookList[indexPath.row - count].dataRegistDate!
            bookData.dataItemPrice = self.searchBookList[indexPath.row - count].dataItemPrice!
            bookData.dataItemCaption = self.searchBookList[indexPath.row - count].dataItemCaption!
            bookData.dataImpression = self.searchBookList[indexPath.row - count].dataImpression!
            bookData.dataAffiliateUrl = self.searchBookList[indexPath.row - count].dataAffiliateUrl!
            bookData.dataReadStartDay = ""
            bookData.dataReadEndDay = ""
            bookData.dataSize = self.searchBookList[indexPath.row - count].dataSize!
            bookData.dataISBN = self.searchBookList[indexPath.row - count].dataISBN!
            bookData.dataLabel = self.searchBookList[indexPath.row - count].dataLabel!
            bookData.dataStatus = dataStatus
            bookData.dataRegistNumber = bookRegistNumber
            bookData.dataCollection = bookTagTextField
            realm.add(bookData)
        }
        bookTableView.reloadData()
        
        let alert = UIAlertController(title: "", message: "本棚に登録しました", preferredStyle: .alert)
        self.present(alert, animated: true, completion: {// アラートを閉じる
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        })
        self.tableView.reloadData()
        
        self.saveBookRegistNumber()
    }
    
    func bookAlertSheet(indexPath:IndexPath){
        let alertController=UIAlertController(title: "本棚に登録", message: "タグと読書状況を入力してください", preferredStyle: UIAlertController.Style.alert)
        var titleName:[String] = ["欲しい","読んでいる","未読","既読","登録をやめる"]
        alertController.addTextField{(textFieldA) -> Void in
            self.textField = textFieldA
            self.textField.delegate = self
            self.textField.placeholder = "タグを入力してください"
            self.textField.textAlignment = .center
            self.textField.isEnabled = false
            
            let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
            let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
            let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let history = UIBarButtonItem(title: "履歴", style: .plain, target: self, action: #selector(self.history))
            toolbar.setItems([doneItem,flexibleItem,history], animated: true)
            self.textField.inputAccessoryView = toolbar
            
            if let tagHistory:[String] = UserDefaults.standard.stringArray(forKey: "TagHistoryArray"){
                self.tagHistoryArray = tagHistory
            }
        }
        let defaultAction = UIAlertAction(title: titleName[0],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(indexPath: indexPath, dataStatus: titleName[0], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let defaultAction1 = UIAlertAction(title: titleName[1],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(indexPath: indexPath, dataStatus: titleName[1], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let defaultAction2 = UIAlertAction(title: titleName[2],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(indexPath: indexPath, dataStatus: titleName[2], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let defaultAction3 = UIAlertAction(title: titleName[3],style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.editActionBookSave(indexPath: indexPath, dataStatus: titleName[3], bookTagTextField: (alertController.textFields?.first?.text)!)
            self.tagHistoryArraySave(text: (alertController.textFields?.first?.text)!)
        })
        let cancel=UIAlertAction(title: titleName[4],style: UIAlertAction.Style.destructive,handler: {
            (action: UIAlertAction!) -> Void in
            self.tableView.reloadData()
        })
        alertController.addAction(defaultAction)
        alertController.addAction(defaultAction1)
        alertController.addAction(defaultAction2)
        alertController.addAction(defaultAction3)
        alertController.addAction(cancel)
        self.present(alertController,animated: true,completion: {
            alertController.textFields?.first?.isEnabled = true
        })
    }
    
    //タグの履歴を保存する処理。
    func tagHistoryArraySave(text: String){
        if(tagHistoryArray.firstIndex(of: text) == nil && text != ""){//履歴にないデータ　かつ　未入力は入れない
            tagHistoryArray.insert(text, at: 0)
            UserDefaults.standard.set(tagHistoryArray, forKey: "TagHistoryArray")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let searchBookDetailView:BookDetailViewController = segue.destination as! BookDetailViewController
        if let indexPath = self.tableView.indexPathForSelectedRow{

            var count = (indexPath.row + 1) % 31
            count = (indexPath.row + 1) - count
            count = count / 31

            searchBookDetailView.image=searchBookList[indexPath.row - count].dataImage!
            searchBookDetailView.detailTitle=searchBookList[indexPath.row - count].dataTitle!
            searchBookDetailView.author=searchBookList[indexPath.row - count].dataAuthor!
            searchBookDetailView.publisher=searchBookList[indexPath.row - count].dataPublisher!
            searchBookDetailView.label = searchBookList[indexPath.row - count].dataLabel!
            searchBookDetailView.size = searchBookList[indexPath.row - count].dataSize!
            searchBookDetailView.salesDate=searchBookList[indexPath.row - count].dataSalesDate!
            searchBookDetailView.itemPrice=searchBookList[indexPath.row - count].dataItemPrice!
            searchBookDetailView.isbn = searchBookList[indexPath.row - count].dataISBN!
            searchBookDetailView.itemCaption=searchBookList[indexPath.row - count].dataItemCaption!
            searchBookDetailView.affiliateUrl=searchBookList[indexPath.row - count].dataAffiliateUrl!
            
            searchBookDetailView.indexPath = indexPath
        }
    }
}


//タグの履歴表示のためのクラス
extension PopularTableViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //表示する数
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return tagHistoryArray.count + 1
    }
    //表示するためのもの
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (row == 0) {
            return ""
        }else{
            return tagHistoryArray[row - 1]
        }
    }
    //選択された時
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row != 0 {
            textField.text = tagHistoryArray[row - 1]
        }
    }
    
    @objc func done() {
        textField.endEditing(true)
    }
    
    @objc func history(){
        //PickerViewの設定
        let pickerView: UIPickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.showsSelectionIndicator = true
        self.textField.inputView = pickerView
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let history = UIBarButtonItem(title: "キー", style: .plain, target: self, action: #selector(self.keyboard))
        toolbar.setItems([doneItem,flexibleItem,history], animated: true)
        self.textField.inputAccessoryView = toolbar
        
        self.textField.reloadInputViews()
    }
    
    @objc func keyboard() {
        //https://starzero.hatenablog.com/entry/2014/08/05/170122 普通のキーボードに戻す処理
        self.textField.inputView = nil
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done))
        let flexibleItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let history = UIBarButtonItem(title: "履歴", style: .plain, target: self, action: #selector(self.history))
        toolbar.setItems([doneItem,flexibleItem,history], animated: true)
        self.textField.inputAccessoryView = toolbar
        
        self.textField.reloadInputViews()
    }
}









