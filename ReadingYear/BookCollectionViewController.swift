//
//  BookCollectionViewController.swift
//  ReadingYear
//
//  Created by 高柳亮太 on 2017/09/28.
//  Copyright © 2017年 高柳亮太. All rights reserved.
//

import UIKit
import RealmSwift
import DZNEmptyDataSet

class BookCollectionViewController: UICollectionViewController,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,UICollectionViewDelegateFlowLayout, UITextFieldDelegate{

    var searchText: String = ""
    var filterText:String = "全て"
    var sortText:String = "dataRegistNumber"
    
    var textField: UITextField!
    var str:[String] = []

    //書籍詳細画面で本を削除を選択した場合に利用する
    var bookDeleteFlag = false
    var selectCellIndexPath = IndexPath(row: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        //遷移先から戻るの時の名前をbackから戻るに変更
        let backButtonItem = UIBarButtonItem(title: "戻る", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem

        
        //https://qiita.com/k_kuni/items/c436ddc3bc9fb3f3aa14
        //viewcontrollerからcollectionviewcontrollerに移動したときになぜかsectionheader部分におかしな空白ができる
        //それの問題点を上記URLで解決
        //レイアウトの設定
        let layout = UICollectionViewFlowLayout()
        let width:CGFloat = (self.collectionView?.frame.width)! / 4 - 6 //行間分の間を確保するための6
        let height:CGFloat = (self.collectionView?.frame.width)! / 3
        layout.itemSize = CGSize(width: width, height: height)
        print(layout.itemSize)
        layout.sectionInset = UIEdgeInsets.init(top: 0,left: 0,bottom: 0,right: 0) //sectionの上左下右の大きさ
        layout.minimumLineSpacing = 8 //行間の大きさ
        layout.minimumInteritemSpacing = 8 //列間の大きさ
        layout.sectionHeadersPinToVisibleBounds = true //headerを固定化
//        layout.headerReferenceSize = CGSize(width: (self.collectionView?.frame.width)!, height: 28)
        self.collectionView?.collectionViewLayout = layout
        
        let color:UIColor=UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        collectionView?.backgroundColor=color
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        let searchButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        searchButton.addTarget(self, action: #selector(tagButtonTap), for: .touchUpInside)
        view.addSubview(searchButton)
        let label = UILabel(frame: CGRect(x: 0, y: 13, width: 44, height: 18))
        label.text = "本棚"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = UIColor.white
        view.addSubview(label)
        let imageView = UIImageView(frame: CGRect(x: 17, y: 30, width: 10, height: 10))
        imageView.image = UIImage(named: "icons8-sort-down-filled-50.png")
        view.addSubview(imageView)
        navigationItem.titleView = view
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //戻った時にセルの選択状態を解除する処理。
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //TODO
        
        //セルの選択状態を解除する処理。　戻った時にどのセルを選択していたかを見やすくするための処理。
        //TODO 削除フラグが立っていた場合には、本をdeleteする処理を行う。
        if (bookDeleteFlag) {
            let realm = try! Realm()
            var result = realm.objects(RealmBookData.self)
            if self.searchText != "" {
                result = result.filter("dataTitle CONTAINS '\(self.searchText)' || dataAuthor CONTAINS '\(self.searchText)' ||  dataPublisher CONTAINS '\(self.searchText)'")
            }else{
                result = self.resultData(indexPath: selectCellIndexPath)
            }
            
            try! realm.write {
                realm.delete(result[selectCellIndexPath.row])
            }
            let alert = UIAlertController(title: "", message: "本を削除しました", preferredStyle: .alert)
            self.present(alert, animated: true, completion: {// アラートを閉じる
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if(result.count == 0){
                        self.collectionView?.reloadData()
                    }else{
                        self.collectionView?.deleteItems(at: [self.selectCellIndexPath])
                    }
                    alert.dismiss(animated: true, completion: nil)
                })
            })
            bookDeleteFlag = false
        }else{
            collectionView?.reloadData()
        }
    }
    
    //TODO タグで処理を制御する処理を追加する
    @objc func tagButtonTap(){
        let alertController = UIAlertController(title: "タグで絞り込み検索", message: "※次回アップデートで機能実装します。\nどうぞ楽しみにお待ちください", preferredStyle: UIAlertController.Style.alert)

        let defaultAction = UIAlertAction(title: "OK",style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
        })
        
        alertController.addAction(defaultAction)
        self.present(alertController,animated: true,completion: nil)
    }
    
    @objc func searchButtonTap(){
        let alertController = UIAlertController(title: "本棚内検索", message: "キーワードを入力してください\n※検索後は空欄にしてください", preferredStyle: UIAlertController.Style.alert)
        
        alertController.addTextField{(textFieldA) -> Void in
            self.textField = textFieldA
            self.textField.delegate = self
            self.textField.placeholder = "タイトル、著者など"
            self.textField.textAlignment = .center
            self.textField.isEnabled = true
            self.textField.text = self.searchText
        }
        
        let defaultAction = UIAlertAction(title: "検索",style: UIAlertAction.Style.default,handler: {
            (action: UIAlertAction!) -> Void in
            self.filterText = "全て"
            self.sortText = "dataRegistNumber"
            self.searchText = (alertController.textFields?.first!.text)! //text_get
            self.collectionView?.reloadData()
        })
        let cancel = UIAlertAction(title: "キャンセル",style: UIAlertAction.Style.cancel,handler: {
            (action: UIAlertAction!) -> Void in
        })
        
        alertController.addAction(defaultAction)
        alertController.addAction(cancel)
        self.present(alertController,animated: true,completion: {
            alertController.textFields?.first?.isEnabled = true
        })
    }
    
    override func viewDidLayoutSubviews() {
        collectionView?.emptyDataSetDelegate = self
        collectionView?.emptyDataSetSource = self
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        let image:UIImage = UIImage(named: "icons8-reading-filled-100.png")!
        return image
    }
    func imageTintColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return UIColor.lightGray
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "本の登録数は0冊です"
        return NSAttributedString(string: text)
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "「本を検索」から本を登録しよう！\n※本は左スワイプで登録できます"
        return NSAttributedString(string: text)
    }
    
    @IBAction func searchButton(_ sender: UIBarButtonItem) {
        searchButtonTap()
    }
    
    @IBAction func filterButton(_ sender: UIBarButtonItem) {
        let alert: UIAlertController = UIAlertController(title: "絞り込み", message: .none, preferredStyle:  UIAlertController.Style.actionSheet)
        var titleName:[String] = ["全て","欲しい","読んでいる","未読","既読"]
        
        let defaultAction: UIAlertAction = UIAlertAction(title: titleName[0], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.filterText = "全て"
            self.collectionView?.reloadData()
        })
        let defaultAction1: UIAlertAction = UIAlertAction(title: titleName[1], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.filterText = "欲しい"
            self.collectionView?.reloadData()
        })
        let defaultAction2: UIAlertAction = UIAlertAction(title: titleName[2], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.filterText = "読んでいる"
            self.collectionView?.reloadData()
        })
        let defaultAction3: UIAlertAction = UIAlertAction(title: titleName[3], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.filterText = "未読"
            self.collectionView?.reloadData()
        })
        let defaultAction4: UIAlertAction = UIAlertAction(title: titleName[4], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.filterText = "既読"
            self.collectionView?.reloadData()
        })
        let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler:{
            (action: UIAlertAction!) -> Void in
        })
        
        if(self.filterText == "全て"){
            defaultAction.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction.setValue(UIColor.orange, forKey: "titleTextColor")
        }else if(self.filterText == "欲しい"){
            defaultAction1.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction1.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction1.setValue(UIColor.orange, forKey: "titleTextColor")
        }else if(self.filterText == "読んでいる"){
            defaultAction2.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction2.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction2.setValue(UIColor.orange, forKey: "titleTextColor")
        }else if(self.filterText == "未読"){
            defaultAction3.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction3.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction3.setValue(UIColor.orange, forKey: "titleTextColor")
        }else{
            defaultAction4.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction4.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction4.setValue(UIColor.orange, forKey: "titleTextColor")
        }
        
        alert.addAction(defaultAction)
        alert.addAction(defaultAction1)
        alert.addAction(defaultAction2)
        alert.addAction(defaultAction3)
        alert.addAction(defaultAction4)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)

    }
    
    @IBAction func sortButton(_ sender: UIBarButtonItem) {
        let alert: UIAlertController = UIAlertController(title: "並び替え", message: .none, preferredStyle:  UIAlertController.Style.actionSheet)
        var titleName:[String] = ["登録順","タイトル順","著者順","タグ順"]

        
        let defaultAction: UIAlertAction = UIAlertAction(title: titleName[0], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.sortText = "dataRegistNumber"
            self.collectionView?.reloadData()
        })
        
        let defaultAction1: UIAlertAction = UIAlertAction(title: titleName[1], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.sortText = "dataTitle"
            self.collectionView?.reloadData()
        })
        let defaultAction2: UIAlertAction = UIAlertAction(title: titleName[2], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.sortText = "dataAuthor"
            self.collectionView?.reloadData()
        })
        let defaultAction3: UIAlertAction = UIAlertAction(title: titleName[3], style: UIAlertAction.Style.default, handler:{
            (action: UIAlertAction!) -> Void in
            self.sortText = "dataCollection"
            self.collectionView?.reloadData()
        })
        let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertAction.Style.cancel, handler:{
            (action: UIAlertAction!) -> Void in
        })
        
        if(self.sortText == "dataRegistNumber"){
            defaultAction.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction.setValue(UIColor.orange, forKey: "titleTextColor")
        }else if(self.sortText == "dataTitle"){
            defaultAction1.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction1.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction1.setValue(UIColor.orange, forKey: "titleTextColor")
        }else if(self.sortText == "dataAuthor"){
            defaultAction2.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction2.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction2.setValue(UIColor.orange, forKey: "titleTextColor")
        }else{
            defaultAction3.setValue(UIImage(named: "todo.png"), forKey: "image")
            defaultAction3.setValue(UIColor.orange, forKey: "imageTintColor")
            defaultAction3.setValue(UIColor.orange, forKey: "titleTextColor")
        }
        
        alert.addAction(defaultAction)
        alert.addAction(defaultAction1)
        alert.addAction(defaultAction2)
        alert.addAction(defaultAction3)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func changeViewButton(_ sender: UIBarButtonItem) {
        let VC = self.tabBarController?.children[0] as? ViewController
        VC?.changeContainerView()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
        if(sortText == "dataCollection"){//ここ超汚い気がする　処理
            self.str = []
            let realm = try! Realm()
            var result:Results<RealmBookData> = realm.objects(RealmBookData.self).sorted(byKeyPath: "dataCollection", ascending: true)
            self.str.append(result[0].dataCollection)//0個の場合クラッシュするのでは？
            var query:String = ""
            if(filterText == "全て"){
                query = "dataCollection != '\(result[0].dataCollection)'"
            }else{
                query = "dataStatus = '\(filterText)' && dataCollection != '\(result[0].dataCollection)'"
            }
            result = realm.objects(RealmBookData.self).filter(query).sorted(byKeyPath: "dataCollection", ascending: true)
            while(result.count != 0){
                self.str.append(result[0].dataCollection)
                query = "\(query) && dataCollection != '\(result[0].dataCollection)'"
                result = realm.objects(RealmBookData.self).filter(query).sorted(byKeyPath: "dataCollection", ascending: true)
            }
            return str.count
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if (sortText == "dataCollection") {
            return CGSize(width: self.view.bounds.width, height: 28)
        }else{
            return CGSize(width: self.view.bounds.width, height: 0)
        }
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let realm = try! Realm() //追加
        let result = realm.objects(RealmBookData.self)
        let indexPath = IndexPath(row: 0, section: section)
        if searchText != "" {
            return result.filter("dataTitle CONTAINS '\(searchText)' || dataAuthor CONTAINS '\(searchText)' || dataPublisher CONTAINS '\(searchText)'").count
        }else{
            return resultData(indexPath: indexPath).count
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookCell", for: indexPath) as! BookCollectionViewCell
        
        let realm = try! Realm()
        var result = realm.objects(RealmBookData.self)
        if searchText != "" { //追加
            result = result.filter("dataTitle CONTAINS '\(searchText)' || dataAuthor CONTAINS '\(searchText)' ||  dataPublisher CONTAINS '\(searchText)'")
        }else{
            result = resultData(indexPath: indexPath)
        }
        let book = result[indexPath.row]
        
        cell.imageView.image = UIImage(data: book.dataImage as Data)
        
        if(book.dataStatus == "既読"){
            cell.bookStatus.image = UIImage()
        }else if(book.dataStatus == "未読"){
            cell.bookStatus.image = UIImage(named: "BookStack.png")
        }else if(book.dataStatus == "欲しい"){
            cell.bookStatus.image = UIImage(named: "Star.png")
        }else{
            cell.bookStatus.image = UIImage(named: "BookRead.png")
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "SectionHeader", for: indexPath) as! SectionHeaderCollectionReusableView
//        header.sectionLabel.text = "\(str[indexPath.section])"
        header.sectionLabel.backgroundColor = UIColor(red: 0, green: 160/255, blue: 1, alpha: 1)
        header.sectionLabel.textColor = UIColor.white
        header.sectionLabel.font = UIFont.systemFont(ofSize: 14)//UIのフォントとサイズ
        if(str[indexPath.section] == ""){
            header.sectionLabel.text = "  未設定"
        }else{
            header.sectionLabel.text = "  " + str[indexPath.section]
        }
        return header
//        return UICollectionReusableView()
    }
    
    //segue遷移先に値渡し
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let bookDetailView:BookShelfDetailViewController = segue.destination as! BookShelfDetailViewController
        if let indexPath = collectionView?.indexPathsForSelectedItems{
            let realm = try! Realm()
            var result = realm.objects(RealmBookData.self)
            if searchText != "" {
                result = result.filter("dataTitle CONTAINS '\(searchText)' || dataAuthor CONTAINS '\(searchText)' ||  dataPublisher CONTAINS '\(searchText)'")
            }else{
                result = resultData(indexPath: indexPath[0])
            }
            //ここの処理怖い。確認してこ。
            let bookData = result[indexPath[0].row]
            bookDetailView.bookData=bookData
            self.selectCellIndexPath = indexPath[0]
        }
    }
    
    func resultData(indexPath: IndexPath) -> Results<RealmBookData>{
        let realm = try! Realm()
        var result:Results<RealmBookData> = realm.objects(RealmBookData.self)
        
        if(filterText == "全て"){//全ての場合はfilterをかけない
            if(sortText == "dataRegistNumber"){
                result = result.sorted(byKeyPath: "\(sortText)", ascending: false)
                
            }else if(sortText == "dataCollection"){
                result = result.filter("dataCollection = '\(str[indexPath.section])'").sorted(byKeyPath: "dataRegistNumber", ascending: false)
            }else{
                result = result.sorted(byKeyPath: "\(sortText)", ascending: true)
            }
        }else{
            if(sortText == "dataRegistNumber"){
                result = result.filter("dataStatus = '\(filterText)'").sorted(byKeyPath: "\(sortText)", ascending: false)
                
            }else if(sortText == "dataCollection"){
                result = result.filter("dataStatus = '\(filterText)' && dataCollection = '\(str[indexPath.section])'").sorted(byKeyPath: "dataRegistNumber", ascending: false)
            }else{
                result = result.filter("dataStatus = '\(filterText)'").sorted(byKeyPath: "\(sortText)", ascending: true)
            }
        }
        
        return result
    }
}
